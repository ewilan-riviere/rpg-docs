# RPG docs

[![vue](https://img.shields.io/static/v1?label=Vue&message=v3&color=4FC08D&style=flat-square&logo=vue.js&logoColor=ffffff)](https://vuejs.org/)
[![tailwindcss](https://img.shields.io/static/v1?label=Tailwind%20CSS&message=v3&color=38B2AC&style=flat-square&logo=tailwind-css&logoColor=ffffff)](https://tailwindcss.com/)

[![node](https://img.shields.io/static/v1?label=NodeJS&message=v16&color=339933&style=flat-square&logo=node.js&logoColor=ffffff)](https://nodejs.org/en)
[![pnpm](https://img.shields.io/static/v1?label=pnpm&message=v7&color=F69220&style=flat-square&logo=pnpm&logoColor=ffffff)](https://pnpm.io)

[![Netlify Status](https://api.netlify.com/api/v1/badges/4717fceb-406e-4b79-b427-df6b18971087/deploy-status)](https://app.netlify.com/sites/rpg-docs/deploys)

> A small website about "Game of Roles" RPG

- Deploy on Netlify: [rpg-docs.netlify.app](https://rpg-docs.netlify.app)
- Based on [Vitest](https://vitest.dev)

## Setup

```bash
pnpm i
```

```bash
pnpm dev
```

## Production

```bash
pnpm build
```
