---
title: Équipement
---

# Équipement

Bien évidemment, votre personnage ne commence pas l’aventure en pagne, il dispose d’un certain nombre d’effets personnels. Ces équipements sont disponibles à la fois à l’unité et en bundle, avec d’autres objets.

![](./images/equipment/equipment-05.webp)

Pour connaître votre équipement de départ, utilisez l’une des deux options suivantes, au choix de chaque joueur :

- Optez pour un kit d’équipement (regroupant plusieurs objets) et ajoutez-y deux autres objets de votre choix.
- Compte tenu du profil de votre personnage, réfléchissez aux possessions qu’il pourrait légitimement posséder et notez jusqu’à dix d’entre elles directement sur votre fiche. Il est évident qu’un mercenaire ne disposera pas d’une monture de guerre mais pourra tout à fait posséder une armure, une épée, un poignard, une couverture, une gourde, un sac à dos, un briquet, une casserole, des cataplasmes, sans oublier une bourse. Un caravanier pourrait disposer de cartes, d’une boussole, de vivres, d’une tente, d’une couverture, d’une dague, de laissez-passer, d’un nécessaire à écrire, d’une loupe et, bien évidemment, d’une bourse bien garnie. Ne vous sentez pas obligé·e de trouver pile dix objets, vous pouvez tout à fait vous limiter dès que vous estimez avoir fait le tour de vos affaires.

Enfin, vous disposez d'1d10 couronnes d'or.

![](./images/equipment/equipment-01.webp)
![](./images/equipment/equipment-02.webp)
![](./images/equipment/equipment-03.webp)
![](./images/equipment/equipment-04.webp)
