---
title: Historique
---

# Historique

## Quand j’étais petit, je faisais

Une fois les compétences définies, le joueur peut demander à les augmenter. Il doit pour cela fournir une explication raisonnable au MJ, telle que « *Dans mon enfance, je devais échapper à des plus grands qui me martyrisaient, donc j’ai appris à courir vite!* ». Le meneur pourra alors autoriser d’augmenter la compétence de 10 points (voire 20 en cas de justification extrêmement pertinente).

Le MJ est invité à encourager ces demandes car elles remplissent l’histoire du joueur. Cependant, à chaque bonus accordé, il appliquera un malus équivalent à une autre compétence déjà acquise, en le justifiant. Par exemple « *Comme tu as passé ton enfance à fuir, tu restais solitaire et tu n’as jamais appris à négocier ou mentir dans des situations stressantes.* »

> Remarque : Vu le faible nombre de compétences disponibles, nous vous conseillons de limiter ces changements à trois maximum (trois bonus et trois malus).

## Phrase de synthèse

Nous avons vu plus haut que votre personnage devait pouvoir se résumer à la phrase « *Il est génial parce que... mais la société a des problèmes avec lui parce que...* ».

Maintenant que vous en savez plus sur ses données chiffrées, ses talents, voire son passé, essayez de compléter cette phrase de synthèse. L’important est qu’elle fournisse une motivation suffisamment forte à votre héros pour qu’il ait décidé de partir à l’aventure plutôt que de rester chez lui.

Notez que vous pouvez tout à fait conserver cette phrase de synthèse pour vous et votre MJ sans forcément en référer aux autres joueurs. Après tout, chaque personnage a ses petits secrets. Si vous êtes d’humeur joueuse, vous pouvez même vous inventer une phrase de synthèse «publique », connue des autres personnages, mais qui est en réalité très différente de la réalité de votre personnage. La littérature regorge de héros qui mentent sur leur origine au commencement de l’aventure, alors pourquoi pas vous ?

### Exemple avec Atlan Grethen de Quirk

« *Mon personnage est génial parce que c’est un noble beau parleur mais il a des problèmes avec la société parce qu’il vit en exil suite à la perte de ses titres et de ses biens.* »

Le nom que vous portez n’est pas le vôtre.

Vous avez le souvenir d’une enfance dans des manoirs ou des châteaux luxueux. Puis il y a eu des cris, un incendie, et un domestique, Zlatsko, qui vous a emmené dans la nuit loin de la violence et des flammes. Un voyage en mer. Et puis plus rien. Votre enfance, ça a été Zlatsko, qui vous a appris dans un endroit inconnu – mais c’était une grande ville – tout ce qu’un noble doit savoir pour survivre. Un jour, il a reconnu quelqu’un dans la rue. Il vous a demandé de fuir aussi loin que possible, ce que vous avez fait.

Voilà cinq ans que vous errez, gagnant votre vie avec vos talents intellectuels, ce qui rapporte peu dans l’arrière-campagne du royaume. La seule chose qui vous lie à votre famille est une dague portant un blason inconnu : un cercle bleu sur fond jaune, traversé d’une épée. Peut-être qu’un jour vous découvrirez la véritable histoire de vos parents.

### Exemple avec Keitra d'Ashanul

« *Mon personnage est génial parce que c’est une gladiatrice
très connue mais la société a des problèmes avec elle parce
qu’elle est recherchée.* »

Vous êtes née esclave de parents désespérés qui vous ont vendue, et destinée à être jetée aux lions dans l’arène d’Aqabah. Mais c’était un mauvais jour pour les lions, car vous avez survécu suffisamment longtemps pour que la foule décide que vous deviez vivre. Dotée d’une solide constitution, vous avez été affectée aux gladiateurs – cachant dans votre entraînement que vous étiez une femme. Vous avez gagné presque tous vos combats et vous vous êtes forgé une solide amitié avec Kaheena d'Ashanul, votre sœur de lutte. Remportant de plus en plus de combats, vous avez parcouru le monde sans pour autant gagner votre liberté.

Un jour en Aria, Kaheena conclut un pacte dont elle ne vous révéla rien pour négocier votre affranchissement. Vous vous êtes échappée de votre condition pour partir loin dans le désert Noir à la recherche d’une cité cachée nommée Irem. Mais vous n’y parvîntes jamais, bloquées par une attaque surprise de la garde du sultan. Kaheena couvrit votre fuite et fut capturée.

En Aqabah, puis en Aria, vous avez tenté maintes fois de retrouver votre sœur d’armes, sans succès. Vous avez décidé de faire votre deuil, et bien que recherchée, de profiter de cette liberté nouvelle au nom de celle qui s’est sacrifiée pour vous.
