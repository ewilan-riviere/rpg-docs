---
title: Fiche de personnage
---

# Fiche de personnage

Essentielle dans votre aventure, cette fiche vous accompagnera tout au long de la campagne. Certaines zones ne sont pas liées à vos caractéristiques mais vous permettre de personnaliser votre héros. Le nom bien sûr, mais également son âge et sa description. Prenez un peu de temps pour songer à ces éléments et, ainsi, faire de votre personnage un être unique. Vous n’avez pas besoin d’en faire tout un roman, un ou deux traits caractéristiques suffiront (couleur des cheveux ou des yeux, corpulence, cicatrice, type de vêtements, attitude physique...).

<a href="./images/character/player.webp" target="_blank" download="player">Télécharger</a>

![player](./images/character/player.webp)
