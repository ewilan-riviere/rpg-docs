---
title: Caractéristiques et compétences
---

# Caractéristiques et compétences

![character](./images/character/aria.webp)

## Les caractéristiques principales

Un personnage dispose de cinq caractéristiques :

- La **Force** est une mesure de la puissance musculaire du personnage. Cette caractéristique donne une idée de ce qu’il peut soulever, porter, pousser ou tirer.
- La **Dextérité** mesure la vivacité, l’agilité et la rapidité physique du personnage.
- L’**Endurance** rend compte de la vitalité et de la santé du personnage.
- L’**Intelligence** décrit les capacités d’apprentissage, de mémorisation et d’analyse de votre personnage. Elle ne remplace pas l’intelligence du joueur, mais gère, à sa place, un certain nombre de tâches fastidieuses.
- Le **Charisme** donne un ordre d’idée de l’apparence générale de votre personnage et de l’impression qu’il fait sur les gens au premier contact.

### Répartition

Répartissez **60 points** dans ces 5 caractéristiques avec un *minimum de 4* et un *maximum de 18*.
Une fois les caractéristiques définies, remplissez **les points de vie**. Ils sont égaux à *l’Endurance avec un maximum de 14*.

Au vu de ces statistiques, réfléchissez avec le joueur à la « profession » de son aventurier. *Soldat* ? *Érudit* ? *Marchand* ? *Diplomate* ?

## Les compétences

L’essentiel des actions pouvant échouer des personnages passe par le recours à des compétences. Chacune d’elles s’exprime en pourcentage et suppose de lancer un dé à cent faces (abrégé en 1d100). Si le résultat du dé est inférieur ou égal à la compétence, l’action est réussie.

> *Exemple* : Atlan tente de convaincre un noble de le laisser entrer à une soirée. Atlan a 80 % en Mentir, convaincre et son joueur lance deux dés. Avec un score de 59, Atlan parvient à convaincre le noble de le laisser passer.

### Renseigner ses compétences

Ce sont vos compétences qui vous permetteront de surmonter les épreuves que votre MJ préféré·e mettra sur votre chemin. Vous allez devoir renseigner vos capacités pour personnaliser son héros en développant davantage certaines compétences.

#### Méthode 1

- Pour chaque compétence, ajoutez les deux caractéristiques liées et multipliez le tout par 2. Il s’agit du scoreinitial de la compétence.
- Une fois tous les scores initiaux renseignés, vous disposez de 50 points à répartir comme bon vous semble, sur la base de 1 pour 1. À la création, aucun personnage ne peut disposer d’une compétence supérieure à 80 %.

> *Exemple* : Atlan dispose toujours d’un score de 13 en Charisme et de 10 en Intelligence. Son score initial en Mentir, convaincre est donc de: (13+10) x 2, soit 46 %. Il décide de dépenser 34 de ses 50 points pour améliorer cette compétence qu’il estime vitale, passant son score à 80 %.

#### Méthode 2

Cette méthode, bien que légèrement plus calculatoire que la première, dispose d’un double avantage : des scores initiaux plus élevés et un personnage directement prêt à l’emploi. Elle a été utilisée pour les prétirés. Pour chaque compétence, faites la moyenne de deux caractéristiques liées, en arrondissant à l’entier inférieur. Multipliez alors ce résultat par 5 pour obtenir le score à affecter à la compétence.

> *Exemple* : Atlan dispose d’un score de 13 en Charisme et de 10 en Intelligence. Son score initial en Mentir, convaincre est donc de : [(13+10)/2] x 5. La moyenne arrondie à l’inférieur de 13 et de 10 est de 11, ce qui, multiplié par 5, lui donne une compétence initiale de Mentir, convaincre de 55 %

### Liste des compétences

- **Artisanat, construire** (*Dextérité*, *Intelligence*) : permet de fabriquer des objets manuellement, pour peu que l’on dispose du matériel et des outils nécessaires.
- **Combat à distance** (*Dextérité*, *Intelligence*) : permet d’utiliser des armes à projectiles (arcs, arbalètes...) ou de lancer (couteau, bolas...). La même compétence est utilisée pour toutes les armes à distance mais le MJ peut imposer un malus de -10 % s’il estime l’arme employée trop exotique pour le personnage.
- **Combat rapproché** (*Force*, *Dextérité*) : permet d’utiliser les armes de corps à corps en combat, qu’elles soient à une ou deux mains. Est également utilisé pour parer si le personnage dispose d’une arme adéquate. Une seule et même compétence de Combat rapproché est utilisée pour toutes les armes de corps à corps mais le MJ peut imposer un malus de -10 % s’il estime l’arme employée trop exotique pour le personnage.
- **Connaissance de la nature** (*Dextérité*, *Intelligence*): mélange de chimie et de biologie, cette compétence permet de reconnaître les éléments en extérieur, tels que les plantes et poisons, ou d’en savoir plus sur les animaux.
- **Connaissance des secrets** (*Intelligence*, *Charisme*) : donne au personnage des indices sur des éléments incompréhensibles, du ressort de la culture générale (histoire, géographie, héraldique, etc.). Peut aussi être utilisé pour en savoir plus sur la magie et sur certaines légendes.
- **Courir, sauter** (*Dextérité*, *Endurance*) : même si son nom ne concerne que ses deux activités principales, cette compétence permet en réalité d’accomplir toutes les actions physiques, y compris la nage ou l’escalade.
- **Discrétion** (*Dextérité*, *Charisme*) : utilisée lorsque vous devez vous déguiser ou vous faire oublier dans l’endroit où vous vous trouvez/rendez. Lorsque vous disposez du matériel adéquat pour vous déguiser, vous pouvez même prendre l’apparence de quelqu’un en particulier si vous réussissez votre jet avec un malus de -20 %. Dans le cadre d’une filature ou d’une poursuite, votre Discrétion s’opposera à la Perception de votre adversaire.
- **Droit** (*Intelligence*, *Charisme*): vous donne accès aux usages administratifs et judiciaires des royaumes que vous traversez. Vous savez ainsi ce qui est illégal ou non et la peine encourue pour les différents crimes et délits.
- **Esquiver** (*Dextérité*, *Intelligence*) : cette compétence est utilisée en combat pour éviter les différents coups qui vous sont portés. Vous ne pouvez pas attaquer le même tour où vous esquivez.
- **Intimider** (*Force*, *Charisme*) : parfois, la parole ne suffit pas et il vous faudra manier la menace, les sous-en- tendus et la grosse voix pour forcer votre auditoire à être d’accord avec vous. Intimider entre en jeu à ce moment-là.
- **Lire, écrire** (*Intelligence*, *Charisme*): sans cette compétence, un héros ne peut absolument rien lire, pamême un panneau indicateur. Lire un livre nécessitune demi-journée par partie distincte.
- **Mentir, convaincre** (*Intelligence*, *Charisme*) : plus subtil qu’Intimider, cette compétence vous permet de rallier votre auditoire à votre avis. Elle est utilisée** dans *toutes* *les négociation*s: commerciales, politiques, entourloupes, etc. Votre interlocuteur ou interlocutrice a droit un jet sous Psychologie pour ne pas se laisser faire.
- **Perception** (*Intelligence*, *Charisme*) : représente la capacité du personnage à capter et analyser rapidement les différentes informations de son environnement.
- **Piloter** (*Dextérité*, *Endurance*) : qu’il s’agisse de monter un cheval, de conduire les étranges machines de Kniga voire de mener un navire, cette compétence vous permet de diriger et/ou manœuvrer tout moyen de locomotion (animal, végétal, mécanique, autre...). La présence de plusieurs personnes sur les grands navires est toujours nécessaire.
- **Psychologie** (*Endurance*, *Intelligence*) : cette compétence vous permet d’avoir un aperçu des attentes des autres individus et de rester droit dans vos bottes quand on tente de vous manipuler. Elle est utilisée pour résister à Mentir, convaincre ou Intimider.
- **Réflexes** (*Dextérité*, *Intelligence*): parfois, vous devrez être vif pour échapper à un éboulement, empêcher une noble d’avaler un poison ou protéger un enfant d’un carreau d’arbalète. Réflexes intervient précisément à ce moment-là. Les combats exploitent aussi cette compétence pour déterminer qui agit en premier.
- **Serrures et pièges** (*Dextérité*, *Endurance*) : vous permet de crocheter ou verrouiller tout type de serrure non magique et de fabriquer ou désamorcer tout type de piège mécanique.
- **Soigner** (*Intelligence*, *Charisme*) : vous permet de soigner les blessés, pour peu que vous ayez le minimum d’outils nécessaires. Un jet réussi guérit 1d6 points de vie mais un jet raté en fait perdre 1d3 de plus à la cible. Cette compétence est également utilisée pour diagnostiquer et soigner les maladies, parfois couplée avec Connaissance de la nature pour créer soi-même le remède.
- **Survie** (*Endurance*, *Intelligence*) : Mélange de sens de l’orientation, de repérage aux étoiles et de débrouillardise, cette compétence vous permet de vous accommoder des lieux que vous ne connaissez pas. Elle est notamment employée pour déterminer si vous trouvez de quoi subvenir à vos besoins en milieu sauvage.
- **Voler** (*Intelligence*, *Charisme*) : compétence préférée des roublards et autre tire-laine, Voler permet au personnage de soulager un propriétaire de ses menues possessions, qu’il s’agisse d’une bourse pleine de pièces, d’une bague ou autres objets. Le MJ peut imposer un malus si l’objet à dérober est particulièrement imposant. La victime du larcin a droit à un jet sous Perception pour prendre le voleur sur le fait.

## Groupe

Selon la constitution de votre groupe, le·a MJ pourra vous accorder des points supplémentaire si cela s'avère nécessaire.

– Si votre groupe est polyvalent, ajoutez 10 points à deux compétences représentatives de la profession du personnage (par exemple Combat rapproché et Combat à distance pour les combattants)
– Si votre groupe est spécialisé, ajoutez 15 points à deux compétences représentatives de la profession du personnage (par exemple Mentir, convaincre et Perception pour les diplomates)
– Si votre groupe est hyper spécialisé, ajoutez 20 points à deux compétences représentatives de la profession du personnage (par exemple Connaissance de la nature et Connaissance des secrets pour les érudits)

Les magiciens ne disposent pas de ces bonus de compétences. En contrepartie ils ont accès à des compétences spécifiques, qui leur permettent entre autres de pratiquer leur magie.

## Compétence spéciale

Chaque personnage dispose d'une compétence spéciale telle que *voir dans l'avenir*, *se faire passer pour n'importe qui*, etc... et selon la puissance de la compétence, le·a MJ définira la limite :

- Fréquence, exemple: une seule fois par jour
- Succès, exemple: 50% avec des conséquences en cas d'échec
- Utilisations, exemple: 3 utilisations maximales

Certaines quêtes telles que les quêtes personnelles peuvent donner des capacités spéciales supplémentaires.
