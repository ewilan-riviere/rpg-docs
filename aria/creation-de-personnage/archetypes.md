---
title: Archétypes
---

# Archétypes

Voici une liste non exhaustive des archétypes que vous pouvez choisir :

## Alchimiste

// TODO

## Aventurièr·e

Très polyvent, l'aventurier connait bien la nature et est capable de survivre seul ou d'aider son groupe.

- Caractéristique principale : aucune
- Connaissance de la nature 80%
- Equiver 70%
- Voler 70%
- Survie 60%
- Armes : arbalète, gourdin
- Armure : cuir rigide

Sans compétences spéciales, celle-ci sont à choisir par le joueur.

## Combattant·e

Soldat d'une ville ou déserteur, le combattant a des capacités élevées dans en combat.

- Caractéristique principale : force
- Combat rapproché 70%
- Courir, sauter 65%
- Esquiver 75%
- Armes : pertuisane, épée courte
- Armure : cuir et métal

Sans compétences spéciales, celle-ci sont à choisir par le joueur.

## Disciple étranger

**Magie**

// TODO

## Mage de l'académie d'Aria

**Magie**

// TODO

## Miséricordieux-se

**Difficile**

**Magie**

Chasseurs de mages, les Miséricordieux sont un ordre implacable dont le but est d'éradiquer les mages indépendants qui risquent de détruire l'équilibre des royaumes, ils subissent un conditionnement mental très fort afin de s'assurer de leur fidélité.

Ils peuvent modéliser la magie tout en étant rompus au différentes formes de combat. Ils ont pratiquement épuisé leurs possibilités en tant que mage mais peuvent s'approprier le pouvoir des autres.

- Caractéristique principale : intelligence
- Modélisation 80% : capacité à modeler la magie invoquée pour voir l'efficience de celle-ci s'améliorer, exemple: affiner une boule de feu afin de lui donner une forme particulière pour améliorer sa vitesse
- Connaissance des secrets 60%
- Lire, écrire 80%
- Voler la magie 30% : récupération de la carte d'un autre magicien (utilisation récente)
- Combat rapproché (épée*) 60%
- Sens de la traque 50% : capacité à ressentir qu'un mage fou est près de vous

Un deck de 10 cartes plus 1 joker forment le jeu d'un Miséricordieux.

*: arme traditionnelle des Miséricordieux, leur compétence ne change pas s'ils utilisent une arme différente.

## Noble

// TODO

## Voleur

// TODO

## Créateur·ice d'automates

// TODO
