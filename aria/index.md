---
title: Aria
page: false
footer: true
---

# Aria

En un lieu dont on a oublié le nom, plusieurs héros sont autour d’un feu. Différentes circonstances vous ont réunis mais une, en particulier, semble commune à chacune et chacun d’entre vous. Vous savez, sans vraiment comprendre pourquoi, que vos passés sont nébuleux, des pans entiers de votre jeunesse, voire des événements récents, sont prisonniers de brumes opaques dont vous ne décelez rien.

Et ce soir, sous des étoiles propices, vous avez décidé de contrer le sort. Avec votre talent, vos connaissances, votre finesse et votre force, vous pourriez vous emparer d’un destin de gloire et de richesse, et peut-être de noblesse. C’est le début d’une grande aventure...
