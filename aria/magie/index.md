# Principes

Tous vos joueurs ne se satisferont probablement pas d’un rôle de combattant, marchand ou diplomate. Certains voudront très certainement s’essayer à la magie. En voici les grandes règles.

## Principe de séparation géographique

Tout comme les langages, les religions ou la philosophie, la magie se pratique différemment suivant le pays.

Cela ne signifie pas qu’un magicien ne pourra pas exercer son pouvoir dans un autre pays mais qu’elle sera enseignée, pratiquée et exercée suivant des rites et des codes différents. Leurs effets seront également différents !

Ainsi, un magicien voyageant de royaume en royaume emploiera toujours la magie qu’il a apprise mais son exercice sera soumis à diverses perturbations, car l’usage qu’il fera de sa magie ne correspondra pas à l’usage communément pratiqué dans le royaume où il se trouve.

## Trois types de merveilleux

Aria distingue communément trois grands types de magie.

### La noble magie

La noble magie, ou tout simplement la magie, la sorcellerie, est enseignée dans les académies des contrées du monde d’Aria. Elle est puissante et requiert une étude constante. Les nobles magies sont pratiquées par les académies d'Aria, de Kniga et de Varna.

### La petite magie

La petite magie, celle des sortilèges, est une pratique rurale ou ancestrale, sans codification, entre bagage culturel et outil du quotidien. Elle inclut la magie runique, la lecture des signes, la magie de la mort.

Le pouvoir des étoiles, qui peut influer le destin, est également considéré comme une petite magie même si Aqabah investit du temps et des efforts pour l’élever au rang des autres nobles magies.

En parallèle, certains individus naissent avec des pouvoirs, comme parler aux animaux. Certains objets peuvent invoquer des esprits et les sorcières du Kohest peuvent, dit-on, créer la vie à partir d’argile. Ces magies non reproductibles, artisanales, et mal comprises, entrent également dans le registre de la petite magie.

### La divine magie

Le troisième type de magie, enfin, est inaccessible aux mortels. Et pour cause il regroupe les bienfaits et les malédictions apportés par les dieux eux-mêmes
