# Alchimie et automates

La noble magie de Kniga est une heureuse fusion de la magie et de la technologie, qui se divise en deux domaines d’études :

- l’alchimie, qui concerne la création de potions et substances (souvent explosives) accomplissant des prodiges
- la mécanique de l’éther et la construction d’automates qui en découle.

## Jouer un alchimiste

Un alchimiste, disciple ou accompli, de Kniga dispose des compétences suivantes :

- Identifier une substance 40 %
- Créer une potion 60 %

Chaque alchimiste connaît également la recette et possède les essences du feu d’Ingramus et du passe-muraille de Karloff qui sont des classiques (consulter le tableau pour la description de ces deux potions). Il a juré lors de son intronisation à l’académie de conserver le secret le plus total sur ses connaissances.

Enfin, l’alchimiste sait parfaitement lire les runes étranges de Kniga, quelle que soit leur difficulté. Cela n’aide cependant en rien la lecture des lettres communes des royaumes d’Aria, d’Aqabah et d’Osmanlie (et n’octroie donc pas la compétence Lire, écrire).

## Le processus alchimique

Identifier une substance est le préalable à la création d’une potion. Un jet réussi sous cette compétence permet de connaître plus ou moins grossièrement l’utilité d’une chose : « C’est une plante toxique », « C’est un caillou qui peut brûler... », etc., donc d’orienter ensuite la création proprement dite.

Créer une potion est l’étape suivante. À partir du moment où l’alchimiste dispose d’une fiole de verre vide (n’importe quel contenant peut faire l’affaire), il essaie de créer une potion avec la substance identifiée. La substance est détruite dans la manipulation mais le succès de l’opération entraîne deux effets distincts :

- Tout d’abord, le meneur de jeu annonce quelle potion l’alchimiste a créée
- Plus important, l’alchimiste possède désormais en lui les essences pures de la potion et peut la recréer à tout moment à condition de posséder une fiole de verre vide, un peu de temps, et de réussir un jet sous le % de difficulté de la potion.

## Potions possibles

Vous trouverez ci-après un tableau avec quelques exemples de potions. Nous avons délibérément laissé certaines cases vides pour que votre alchimiste découvre les plantes et expérimente leurs effets en leur donnant son nom.

| Nom                              | Effet                                                                                                                                   | Composant      | Difficulté |
| -------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------- | -------------- | ---------- |
| Le feu ardent d’Ingramus le sage | Bien secouer avant utilisation. Lancer la potion dans un endroit éloigné et être prêt pour une grosse explosion.                        |                | 50         |
| Le passe-muraille de Karloff     | Dissout tout, sauf le verre.                                                                                                            |                | 60         |
| Potion de chance                 | Nécessite de la chair d'un embryon de vouivre de désert, +20% à tous ses jets pendant une semaine et peut relancer ses échecs critiques |                | 50         |
| À définir par l’alchimiste       | Petit soin (mais donne la peau dorée)                                                                                                   | Mérilia        | 60         |
| À définir par l’alchimiste       | Grande malchance                                                                                                                        | Automnale      | 40         |
| À définir par l’alchimiste       | Fait pousser les cheveux abondamment en quelques secondes (baume ! ne pas boire !)                                                      | Peltrille      | 40         |
| À définir par l’alchimiste       | Endormissement (et rêves cauchemardesques)                                                                                              | Vesperante     | 50         |
| À définir par l’alchimiste       | Donne une voix très grave                                                                                                               | Naranjule      | 50         |
| À définir par l’alchimiste       | Nuage d’un noir absolu (et on en ressort tout noir)                                                                                     | Gothica        | 70         |
| À définir par l’alchimiste       | Sentiment d’euphorie extatique (cocaïne)                                                                                                | Follerouge     | 70         |
| À définir par l’alchimiste       | Coliques                                                                                                                                | Boulinette     | 80         |
| À définir par l’alchimiste       | Apparition d’un grand monstre illusoire dans une flamme violette                                                                        | Glodonia       | 50         |
| À définir par l’alchimiste       | Flash aveuglant                                                                                                                         | Feu éternel    | 60         |
| À définir par l’alchimiste       | Lumière diffuse prolongée                                                                                                               | Lumivert       | 70         |
| À définir par l’alchimiste       | Colle à distance (sympathisme)                                                                                                          | Ketioum        | 60         |
| À définir par l’alchimiste       | Rend transparent                                                                                                                        | Bronzolite     | 60         |
| À définir par l’alchimiste       | Pensées à distance (variable)                                                                                                           | Kolesh laineux | 30         |
