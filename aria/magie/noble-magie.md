# Les nobles magies

Dans la version courante d’ARIA, vous pouvez incarner trois types de magiciens pratiquant la noble magie d’Aria, mais dans Préludes, nous vous proposons de découvrir la magie par le prisme d’un archétype unique: le disciple.

> Magicien de la couronne ou disciple de l’académie
>
>Ce personnage a en général une bonne capacité à modéliser la magie, mais il a moins de potentiel magique que les autres types. Comme il est publiquement reconnu, beaucoup de portes et de lieux magiques lui sont ouverts, que ce soit en Aria ou dans le monde entier. Il doit se conformer à un code moral et obéir aveuglément à la couronne, sous peine d’être chassé par les miséricordieux, des mages guerriers veillant sur les leurs.

## Les quatre visages de la noble magie d’Aria

Un personnage doit avoir au moins 14 en Intelligence pour pouvoir être magicien. Son joueur doit en outre disposer d’un paquet de cartes à jouer standard de 52 cartes, auquel il ajoute un joker. Un disciple de l’académie dispose des compétences spéciales suivantes :

- Modélisation 60 %
- Connaissance des secrets 60 %
- Lire, écrire 80 %

S’ils sont supérieurs à ceux calculés au préalable, les scores ci-dessus les remplacent.

Le magicien commence en outre la campagne avec 25 cartes prises au hasard dans le paquet (qu’il ne peut pas regarder), plus le joker.

Durant la partie, le joueur dispose de son paquet (qu’il peut mélanger à tout moment), face cachée. S’il décide d’utiliser la magie, il en prend la première carte, la regarde et l’utilise (ou la jette). Dans tous les cas, la carte est retirée de la partie et perdue à jamais. Plus la carte a une valeur élevée et plus l’effet est important. L’as est la carte la plus forte de sa couleur. Quant au joker, il est tout-puissant: jouer un joker peut tout aussi bien précipiter une ville sous la mer que faire s’écraser la lune sur la surface.

## Magie brute

La magie brute est la magie exploitée par les disciples étrangers à l’académie. Pratiquée sans notion de modélisation, elle utilise les familles des cartes pour prendre forme :

- Les carreaux permettent de manipuler la matière existante (creuser un trou, soulever une vague...)
- Les trèfles matérialisent un des quatre éléments ex nihilo (une boule de feu, une colonne d’eau, un vent puissant...)
- Les piques affectent le monde spirituel (chasser un fantôme, réanimer un cadavre...)
- Les cœurs, enfin, affectent le sentiment des êtres vivants (provoquer la peur ou le malaise, l’amour...)

Il s’agit là des quatre visages de la noble magie d’Aria.
Vous trouverez page suivante quelques exemples d’effets en fonction des familles et de la valeur des cartes.
Ces exemples vous serviront également de base pour calibrer les effets voulus par vos personnages magiciens et éviter qu’ils déclenchent l’Apocalypse en ayant tiré un simple 2.

Lorsqu’un magicien fait appel à la magie brute, il décrit au MJ l’effet général qu’il souhaite obtenir. Mais comme il est incapable de le modéliser, c’est au MJ de décrire l’effet et ses conséquences, qui pourront être parfois... surprenantes.

![cards](./images/cards.webp)

## Modélisation

La magie brute décrite plus haut permet des actions assez rudimentaires et grossières, même si elles peuvent être puissantes. En outre, à moins d’être dans une zone où la magie n’a pas d’effet, la magie brute fonctionne toujours à 100 %.

On peut néanmoins lui donner un autre effet, plus sophistiqué. Un tel effet est souvent cosmétique et vain, mais il montre la maîtrise du magicien. Certes, convoquer une vague géante, tous les magiciens peuvent le faire, mais une vague qui aurait la forme d’un troupeau de chevaux sauvages rend quand même mieux, non? C’est là qu’intervient la compétence modélisation.

Modélisation: cette compétence permet d’adapter le flot de magie brute à l’exploitation qu’en prévoit le magicien. Après avoir utilisé la carte, un jet sous Modélisation réussi (et plus encore si le résultat est inférieur ou égal à 05 %) donnera la possibilité au magicien de modeler parfaitement son effet.

Un jet sous Modélisation réussi peut même augmenter les effets de la magie. Par exemple, si vous voulez projeter une boule de terre contre une porte fermée, lui donner la forme modélisée d’un poinçon et la structure du platine peut en accentuer l’efficacité. De même, vous impressionnerez bien plus votre interlocuteur en allumant une bougie grâce à des flammes en forme de crâne.

La modélisation des cartes de cœur permettrait même d’influencer les pensées. Seulement, l’éthique en vigueur chez les magiciens interdisant une telle action, cette modélisation en reste pour le moment au stade théorique.
