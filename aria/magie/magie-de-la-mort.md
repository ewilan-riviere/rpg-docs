# La magie de la mort

Cette ancienne magie perdue est liée à l’Araignée, le dieu Môn oublié de la mort.

Avant de tuer un être vivant, la personne pratiquant cette magie peut dédier l’âme de la victime à l’Araignée et ainsi, gagner des points de mort. La magie de la mort fonctionne comme suit.

Au préalable, le magicien de la mort adresse une prière silencieuse à l’Araignée. Dans l'heure qui suit, il tue ou fait tuer les victimes qu'il a offert. La manière de mourir importe peu, du moment que les victimes passent de vie à trépas. Chaque animal tué apporte 3 points et chaque être humain, 10 points. Si le magicien prend soin en outre de tuer simultanément des mâles et des femelles de la même espèce, il gagne 5 points supplémentaires – l’Araignée est une divinité égalitariste. L’ensemble de ces points constitue la réserve de points de mort. Plus les points de morts sont élevés, et plus une aura sinistre enveloppe le magicien. Tous ses jets sociaux – et non, le combat n’est pas une compétence sociale – subissent un malus égal à ses points de mort.

À tout moment, le magicien peut tenter un jet sous ses points de mort cumulés pour lancer un sort sur sa compétence de magie de la mort. Une personne portant la marque de l’Ennemi bénéficie d’un bonus de +10 % à ce jet. En cas de succès peut lancer l’un des sorts ci-après. En cas d’échec, le lanceur de sort perd 5 points de mort.

:::warning Règles

- Dédier l'âme de la victime à l'Araignée **avant** sa mort, il n'est pas obligé de tuer lui-même les êtres vivants concernés.
- Tuer la victime dans l'heure, si tel n'est pas le cas, l'Araignée pourra se montra très mécontente.
- Un magicien de la mort cumulant beaucoup de points de mort aura une aura malsaine autour de lui.
- Un échec fait perdre 5 points de magie de la mort, s'ils sont disponibles.
- Les compétences sont débloquées selon le niveau de la compétence.
- En cas de succès le magicien lance un sort de magie de la mort selon les conditions de ce sort.

:::

## Convoquer et parler avec un mort

- Coût : 5 points
- Compétence nécessaire : 10%

Ce sort ne fonctionne que sur le lieu de décès du défunt. Pendant une heure, le personnage peut s’entretenir avec un mort précis, qui revient sous la forme de fantôme. Les autres personnes voient le fantôme mais ne l’entendent pas. Seul le magicien est en mesure de communiquer avec le défunt.

## Envoyer des cauchemars

- Coût : 10 points
- Compétence nécessaire : 20%

Le magicien choisit jusqu’à cinq personnes qu’il connaît, même légèrement. Pendant un mois, ces dernières sont hantées chaque nuit par des mauvais rêves en rapport avec leurs actions passées inavouables. Trop fatiguées, elles perdent en réactivité et en intelligence. Tous leurs jets se font à -20 %.

## Blesser une personne

- Coût : 15 points
- Compétence nécessaire : 30%

Le magicien désigne une personne qu’il peut voir. Cette dernière est aussitôt assaillie par les mânes du fleuve des morts et perd 1d4+1 points de vie. Si ce sort est lancé de nuit ou à proximité d’un champ de bataille, la victime perd 1d6+2 points de vie. Il est impossible de tuer une personne avec ce sort – sinon, il se serait appelé « Tuer une personne ».

## Demander un conseil aux esprits

- Coût : 10 points
- Compétence nécessaire : 40%

Ce sort est plus global que « Convoquer et parler avec un mort ». Il fait appel à la communauté des âmes défuntes, qui répondent collectivement au magicien. Seul ce dernier est en mesure de voir les esprits qui l’entourent, les autres personnes ne ressentant qu’une aura oppressante. Les esprits ne soufflent jamais une solution directement mais orientent correctement les options du lanceur de sort.

## Maudire une personne

- Coût : 20 points
- Compétence nécessaire : 50%

Le magicien choisit une personne dont il connaît le nom. Pendant les cinq prochains jours, cette dernière échoue à tout jet de compétence dont le résultat est pair.

## Réveiller les esprits

- Coût : 20 points
- Compétence nécessaire : 50%

Ce sort est une variante plus puissante de « Demander un conseil aux esprits ». Les âmes mortes se matérialisent tout autour du personnage et figent d’effroi toutes les personnes qui ne sont pas magiciennes de la mort. Un jet sous Intelligence x 3 est nécessaire pour surmonter sa peur.

## Réveiller un cadavre

- Coût : 30 points
- Compétence nécessaire : 60%

Le magicien réveille un cadavre pendant une nuit complète. Jusqu’au lever du jour, le mort sera en pleine possession de ses moyens et de son intelligence – il a également conscience de son statut de mort revenu temporairement parmi les vivants. Il pourra répondre au personnage et même accomplir les missions qui lui sont confiées mais peut tout à fait négocier ce service. À l’aube, le mort s’écroule où il se trouve pour reprendre le sommeil qu’il croyait éternel.
