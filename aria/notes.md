---
title: Notes
page: false
footer: true
---

# Notes

Cette campagne a été menée par un groupe de joueuses : LesSombres™. Si les livres de campagne ont été globalment suivis, la GM a dû parfois proposer de nouvelles règles qui doivent être compulsées pour faire preuve de loi, c'est donc ici qu'elles le seront.

## Compétences spéciales

- Yoni : Manipulation outrageuse pour obtenir presque n'importe quoi par son charme (limité à un être vivant pouvant communiquer)
  - Limites : 50%, 1 fois/session, maximum 2 fois/être vivant
- Isilwen : Regard foudroyant pour mettre la pression sur l'être vivant concerné
  - Limites : 50%, 1 fois/session, maximum 2 fois/être vivant
- Pépa : Alchimiste
  - Le feu ardent d’Ingramus le sage à 50%
  - Le passe-muraille de Karloff à 60%
  - Potion de chance (nécessite de la chair d'un embryon de vouivre de désert) à 50%
    - +20% à tous ses jets pendant une semaine et peut relancer ses échecs critiques

## Magie de la mort

La magie de la mort a été modifiée pour mieux répondre aux besoins de la campagne. Celle-ci est détaillée sur la [page correspondante dans la catégorie Magie](/aria/magie/magie-de-la-mort).

Isilwen possède la magie de la mort à 35%.

- Convoquer et parler avec un mort (5 pts)
- Envoyer des cauchemars (10 pts)
- Blesser une personne (15 pts)

## Guilde des voleurs (aka la guilde des Poissonniers)

Pour monter en rang dans la guilde, tout membre doit d'abord faire ses preuves, seuls les Agents (rang 2 et les rangs au-dessus) ont accès au QG de la guilde et à la plupart des services. Les contrats à effectuer dépendent du rang du voleur.

:::info Règles

- 10% de la valeur des vols offerte au Dieu Poisson
- Fidélité, toute trahison est punie de mort
- Probité, offrir son aide à tout membre de la guilde
:::

:::tip Rangs

- **Novice** (rang 0), fait des petits larcins pour certains Artisans afin de faire leur preuves
- **Apprenti·e** (rang 1), s'occupe de petits boulots précis pour des membres plus élevés dans la guilde
- **Agent·e** (rang 2), voleur d'envergure ayant accès aux services de la guilde ainsi qu'au QG, effectue des contrats auprès des Artisans
- **Compagon/Compagne** (rang 3), voleur important, a accès à tous les receleurs privés de la guilde
- **Artisan·e** (rang 4), membre notoire de la guilde, propose généralement des contrats ou des services spécifiques
- **Maître·sse voleur·se** (rang 5), seulement quelques personnes peuvent se targuer d'être Maîtres dans la guilde, ils sont souvent les chef·fes des guildes locales de chaque ville ou des personnalités très importantes
- **Chef·fe de la guilde des voleurs** (rang 6), actuellement Rupert le Requin, résidant à Aria, il a tout pouvoir sur la guilde des voleurs
:::

:::warning Membres connus

- Aristarque, un charlatan de la place du marché qui trouve de jeunes talents pour la guide des voleurs, toujours habillé de rouge. *Rang Compagon* situé à Aria.
- Astrid aux deux visages, une redoutable agente de la guilde des voleurs, apprécie beaucoup la truite à l'Arbalète, la meilleure taverne d'Aria. *Rang Artisane* situé à Aria.
- Rupert le Requin, le chef de la guilde des voleurs. *Rang Chef de la guilde des voleurs* situé à Aria.
- Jotun, un membre loyal de la guilde des voleurs qui n'a pas été ravi de recroiser le groupe qui l'avait abandonné dans une fillette. *Rang Maître voleur* situé à Aria.
- Salil Al Fara, le prêteur sur gage se révèle être également receleur pour la guilde des voleurs. *Rang Artisan* situé à Aria.
:::
