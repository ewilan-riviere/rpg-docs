---
title: Races jouables
---

# Races jouables

Le monde d'Aria est peuplé de nombreuse créatures pacifiques et dangereuses mais les hommes sont aussi divers que ces créatures. C'est ici que sont référencés les différentes races et hommes hors normes. Presque toutes les races exotiques sont originaires d'Osmanlie suite à des altérations de la race humaine apparues après un cataclysme de magie, "*la clarté de Tansanli*", qui a suivi la chute du Khan. Les races présentées ici peuvent être choisie pour le personnage de départ.

<!--more-->

## Humain

La plupart des habitants de ce monde sont des humains tout à fait ordinaires.

## Melanassi

Les Melanassi ou noirs-humains sont de hommes grands, couverts de poils noirs de la tête aux pieds, et généralement plus dodus que la norme car ils ont un grand appétit. Ils ont vu leur peau se rider de profondes striures noires et leur pilosité se développer jusqu'à ce que leurs yeux sombres disparaissent sous leurs sourcils généreux et qu'il leur devint coutume de natter leurs cheveux interminables pour les lier autour de la ceinture. Ils parlent la noire-langue, et leur histoire est liée à celle des Osmanliens.

## Lynassi

Les Lynassi, homme de corpulence normale, à la peau mate, aux yeux jaunes dorés allongés, dont l'absence de pupille leurs donnent un regards énigmatiques, et avec les cheveux rouges sang (Dans la première campagne la peau étais doré et les yeux rouges; cela a été changé). Ils sont réputés pour leur intelligence prodigieuse, ce qui les rends malicieux et en a fait des légendes que tous aujourd'hui préfèrent éviter. Ils sont liés à l'Osmanli

## Ulassi

Les Ulassi, aussi nommés "blancs-humains", sont des êtres à la peau diaphane, au travers de laquelle on voit pulser les veines de sang rouge et bleu. Leurs yeux sont transparents et leurs chevelures blanches. Ils vivent en Osmanlie, sur les rives de la mer Marmoréenne.

On raconte que c'est Dagan en personne qui a créé la race.

## Elfe noir

Les elfes noirs, sont une race d'humanoïdes à la peau sombre anthracite ou bleu nuit, et aux longues oreilles, ainsi qu'aux cheveux blancs.

**Révèle du lore**

La seul Divinité des elfes noirs et Urshanabi en noire-langue, en tant que Dieu psychopompe, il guide les âmes sur le fleuve des morts jusqu'à l'Haliaslanlar, "*l'au-delà*" des elfes noirs, qui est aussi derrière les monts du Gonen. Ils sont probablement originaire du Delta du Kuthaya.

## Homme-chat

Les hommes-chats sont une race de félin humanoïde, très présent dans la cité-empire de Varna où ils sont respectés , faisant partis des gardes royaux avec des humains aux casques aux oreilles de chat.

**Révèle du lore**

Les félin sont révérés dans la grande cité ce qui explique la présence importante de cette race à l’intérieur de la muraille.

La déesse Bast ou Dieu chat est probablement liée à l’étrange peuple des hommes-chats, miliciens à Varna, qui sont particulièrement vigilants et dévoués et à ce titre forment les milices de l'état-empire.

## Peuple Môn

Le peuple Môn est un ancien peuple disparu ayant vécu sur les terres du Pays Môn, bien avant que celle-ci appartient au royaume d'Aria. Certains rites ce sont mêlés à la culture d'Aria comme le serment de l'Oie sacrée par exemple. Les descendants de ce peuple sont de petite taille et vivent au pays Môn.
