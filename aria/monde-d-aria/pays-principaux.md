---
title: Pays principaux
---

# Pays principaux

Le jeu de rôle d'Aria présente un grand nombre de royaumes différents les uns des autres. Comme la majorité des habitants disposent d'un accès à l'éducation assez limité et que nombre d'entre-eux ne savent pas lire et écrire, ainsi les cartes détaillées du monde sont très rares et la plupart présente des approximations. Votre personnage doit venir d'un de ces pays même s'il n'y est pas retourné depuis très longtemps.

<!--more-->

> Une fois votre personnage validé, le·a MJ vous donnera des informations supplémentaires sur le pays choisi que vous seul·e connaitrez car vous avez vécu dans ce pays.

![map](./images/map.webp)

Les royaumes présentées ci-dessous sont connus par l'essentiel de la population :

## Aqabah

Ce sultanat situé au ponant de la mer de la Morsure est séparé d'Aria par un simple détroit. Les deux nations sont actuellement en guerre pour une histoire de sceptre.

![aqabah](./images/aqabah.webp)

## Altabianca

Cité-État insulaire, Altabianca se trouve au carrefour des royaumes de la mer de la Morsure et jouit à ce titre d'une opulence à faire pâlir d'envie ses voisins

![altabianca](./images/altabianca.webp)

## Aria

Royaume le plus étendu de la mer de la Morsure, Aria domine tout le septentrion, grâce notamment à ses multiples ports et à sa capitale située à l'embouchure du fleuve du Parcoul.

![aria](./images/aria.webp)

## Esperanza

Île de pirates perpétuellement dissimulée par un voile de brume, Esperanza se situe à mi-chemin d'Aqabah et d'Altabianca, et joue ses cartes dans les deux royaumes, voire davantage.

![esperanza](./images/esperanza.webp)

## Kniga

Loin au midi de la mer de la Morsure, cette nation est source de nombreuses légendes. Alchimie, automates, royaumes suspendus... Bien peu sont ceux qui peuvent témoigner de toutes ses merveilles.

![kniga](./images/kniga.webp)

## Osmanlie

Pays ayant sombré dans l'anarchie il y a des années, l'Osmanlie est désormais séparée du reste du monde par un vaste mur. Les gens l'appellent souvent le « sombre pays » et l'on raconte que de fabuleuses créatures y marchent parmi les hommes.

![osmanlie](./images/osmanlie.webp)

## Pays Môn

Au septentrion d'Aria, le pays Môn est un pays pluvieux et verdoyant, où des clans épars tentent désespérement de se fédérer contre un envahisseur venu du froid.

![pays-mon](./images/pays-mon.webp)

## Varna

Cité-État voisine d'Aria, Varna concentre son activité sur la défense du mur la séparant de l'Osmanlie. Elle est réputée pour la sagesse et la clairvoyance de son roi.

![varna](./images/varna.webp)
