# Saison 01 : La guerre des deux royaumes

Ces épisodes se sont produits après [les préludes](/gamelog/aria/preludes) et l'équipe de LesSombres™ s'est réunie.

## 1. Lacigüe, c'pas cool

:::info Joueuses
Isilwen, Pepa, Yoni
:::

**Suite aux évènements avec les bandits, nos héroïnes arrivent à Arance pour mener l'enquête sur un meurtre.**

En effet, Roget le bourgmestre du village fait face à un meurtre, celui du marchand Eudes. Il a trouvé trois suspects et s'apprête à les pendre quand le groupe arrive, comme le bourgmestre est particulièrement couard, il préfère proposer au groupe de mener son enquête avec un regard extérieur et d'exécuter eux-mêmes la sentence, en tant que représentant d'Aria.

Après une enquête minutieuse, il s'avère qu'Eudes battait sa femme, Hermance, et que celle-ci a voulu le tuer en demandant un poison terrible à la rebouteuse du village, Esclarmide. Celle-ci a fourni une potion qui devait juste donner des maux de ventre afin de calmer l'agressivité de son mari. Sauf que le marchand avait également engagé un nouveau domestique, Baudouin, un charpentier de formation qui croulait sous les dettes (après quelques manipulations de la part d'Eudes), et celui-ci a dû préparer le dîner le même jour. C'est lors de la préparation de ce repas que Baudouin a confondu de la ciguë avec des herbes aromatiques et a préparé, sans le savoir, un poulet aux herbes mortel.

Le groupe va donc défendre les suspects tout en extorquant quelques récompenses, en particulier de la part d'Hermance, qui a dû faire valoir chèrement son innocence. Mais nos héroïnes ne s'arrêteront pas là et feront pression sur Roget pour que celui-ci cesse également de maltraiter sa femme en indiquant à quel point il avait mal géré la crise.

Après cette quête, LesSombres™ se sont dirigées vers le village de Claveau, au nord près de la montagne du Dieu de la Sagesse, pour comprendre pourquoi celui-ci ne livrait plus de fer au territoire du Rideau (quête donnée par Roget). Sur la route, le groupe laissera Jotun dans une fillette et le nouveau cheval de Pépa, offert par Hermance, tombera dans le Parcoul.

Là-bas, le groupe découvrira un village sous l'oppression d'un bandit Osmalien nommé Caldera qui souhaite faire de Claveau un village indépendant et accroitre sa propre richesse. Il est accompagné par sa bande de pillards qui l'aident à maintenir la population et faire exécuter ses ordres. Mais Julia, une jeune femme locale, tente de s'opposer au règne des bandits et tente de rallier le groupe à sa cause, c'est ainsi que nos héroïnes vont enquêter dans la nuit et se faire attrapper par les membres de la *troupe d'artistes* de Caldera, sauf Pépa qui parvient à se cacher. C'est donc Yoni puis Isilwen qui paraitront devant le bandit pour justifier de leur présence et comprendre les intentions des nouveaux maîtres des lieux.

Yoni qui est une lynassi séduira Caldera (également un lynassi) et entamera des ébats avec Xara, une jeune femme de la bande, et le chef des bandits. Isilwen se fera un ami de Corekciler lors d'un concours de beignets. Toutes deux seront traitées avec respect par les bandits et comprennent assez vite que leur but est de les amener à partager leurs ambitions. Il est notable de souligner que les bandits possèdent un trésor assez impressionant.

:::tip Récompenses

- Soieries de la part d'Hermance
- Des couronnes extorquées à Hermance
- Des couronnes offertes par Roget
- Le retrait de Roget de la vie politique
- L'hygiène de la rebouteuse
- Amélioration de la vie locale
- Hermano, un cheval pour Pépa
- Bonne quantité de ciguë
:::

:::warning Personnages

- Roget, bourgmestre d'Arance
- Flora, femme du bourgmestre Roget
- Eudes (non-rencontré), le marchand qui a été assassiné et qui battait sa femme Hermance
- Hermance, la femme du marchand assassiné, a commandé une potion à la rebouteuse pour tuer son mari
- Baudouin, domestique en service obligatoire d'Eudes, charpentier de profession, a tué son maître par accident avec de la cigüe après avoir été forcé à préparer le repas
- Esclarmide, rebouteuse du village, couverte de crasse, elle a fait une potion pour rendre malade le mari d'Hermance
- Gédéon (non-rencontré), sénéchal de Claveau, suspecté de haute trahison
- Jotun, bandit laissé dans une cage sur le chemin de Claveau
- Julia, une habitante de Claveau, révoltée par la politique de la bande de Caldera, a un zozottement
- Caldera, chef de la bande de brigands qui a fait main basse sur le village, lynassi, amant de Yoni
- Corekciler, énorme bandit de la bande de Caldera, melanassi, adore les beignets, nouvel ami d'Isilwen
- Avelina, bandite de la bande de Caldera, femme d'arme, soeur de Xara, très loyale à Caldera
- Xara, bandite de la bande de Caldera, amante occasionnelle de celui-ci, amante de Yoni, soeur d'Avelina, voleuse
:::

:::danger Pertes

- Hermano, le cheval de Pépa, a été emporté par le fleuve
:::

## 2. Séduction empoisonnée

:::info Joueuses
Isilwen, Pepa, Yoni
:::

**Suite aux évènements lors de leur arrivée à Claveau, nos héroïnes trouvent le moyen d'empoisonner "l'artiste" Caldera et (une partie) de sa troupe.**

Après une nuit de débauche pour Yoni, une nuit de camaraderie pour Isilwen et une nuit dans les écuries pour Pépa, nos trois héroïnes décident d'aider le village à leur manière. Elles apprennent que Gédéon, le sénéchal de Claveau, s'est "noyé" dans le Parcoul et elles partagent leurs informations sur les bandits et la situation du village avec Julia. C'est là que le groupe lui affirme qu'elle devrait être la cheffe de Claveau quand le village sera libéré. Elles tentent plusieurs approches pour tester la loyauté des bandits de la bande, notamment envers Xara dont Yoni s'est entichée.  Elles vont beaucoup discuter avec les membres notables de la bande comme Caldera, Xara, Avelina et Corekciler pour apprendre de nombreuses informations sur l'Osmanlie et leur vie de pillage. Yoni se lie davantage avec Xara au cours de leurs discussions sur sa vie antérieure.

Et elles finissent par fomenter un complot à base de poulet aux herbes spécialité ciguë, le repas sera préparé par Pépa avec une aide de la part d'Isilwen pendant que Yoni tentera de capter l'attention de Xara et de sa soeur, Avelina, afin que celles-ci ne meurent pas avec le reste de la bande. La préparation prendra une journée entière et semée d'embûches notable comme le moment affreux où la sauce s'est retrouvée être un échec. Mais nos débrouillardes héroïnes parviennent à réaliser un plat d'exception pendant que Yoni prépare la seconde partie du plan : faire croire à un empoisonnement de la part du tavernier de la ville, Ernoulet qui a été anarqué par un des bandit. Elle lui demande donc d'apporter de la bière afin de rendre le repas plus convivial, celui-ci fini par accepter devant le bagou et l'or de Yoni. L'idée est simple, si Ernoulet est déclaré coupable, Xara et Avelina, les seules rescapées suivront alors nos héroïnes et seront leurs alliées.

La ciguë est mise dans la sauce et le repas est servi aux bandits qui se réjouissent d'un repas de qualité, Isilwen et Pépa tente de juguler l'attente alors que les bières supposément empoisonnées arrivent. Alors que Yoni fait tout pour garder Xara et sa soeur dehors pour éviter qu'elles ne partagent le repas empoisonné. C'est en proposant un toast que nos deux assassins arrivent à faire boire de la bière aux bandits avant le repas pour que ceux-ci meurent tous quelque instant plus tard après avoir goûté à la spécialité de Pépa. S'ensuit alors un concours de la meilleure actrice dramatique entre Pépa et Isilwen qui s'effondrent en criant devant cet empoisonnement fictif et réel. Elles évoquent la bière, Yoni s'empare alors du sujet et proclame qu'elle doit venger Caldera avant de sortir en laissant ses deux comparses jouer la comédie autour de Xara et Avelina, en pleurs.

Yoni va donc rejoindre Ernoulet, qu'elle assassine impunément pour que celui-ci ne puisse pas révéler la vérité et c'est alors qu'elle a les mains recouvertes de sang que Xara la rejoint, voulant venger ses compagons. Elle tombe dans les bras de sa "sauveuse" et accepte de rejoindre le groupe, elle et sa soeur.

La soirée se finira par un bucher funéraire, l'enterrement des preuves et la répartion des richesses des bandits. Le groupe se partage également un embryon de vouivre du désert en brisant l'oeuf découvert dans le manoir, chaque membre de groupe d'origine, y compris le frère de Yoni, Linga, vont se voir attribuer des cartes de magie. Le lendemain matin, le chevalier Garnier du Rideau débarquera avec une troupe de soldats pour libérer le village. Alors que les militaires arrivent, le groupe réquisitionne un chariot pour y déverser rapidement leur butin et le recouvrir de minerai de fer. Leur plan est presque parfait en-dehors des villageois qui dénoncent Xara et Avelina comme étant des bandits, alors que Garnier va tenter de procéder avec négociation, Isilwen utilise sa compétence de Regard foudroyant pour sauver leur groupe et les faire quitter le village en toute liberté avec leurs nouvelles richesses et compagnes.

Elles repasseront par Arance pour s'apercevoir que Flora dirige maintenant le village et que Roget n'ose plus sortir de chez lui. Après une tentative infructueuse pour vendre le minerai de fer (qui ne sert à rien aux gens d'Arance), elles se dirigeront vers Aria pendant plusieurs jours. Une fois arrivées à la capitale, elles s'intéressent de près au marché de l'immobilier et trouvent un notaire, Elias, qui propose plusieurs biens à vendre. C'est son assistant, Philodias Plaza, qui va effectuer les visites des diverses propriétés disponibles sur le marché et c'est une maison dans le quartier des plaisirs qui semble retenir l'attention du groupe.

:::danger Pertes

- Cigüe pour la sauce du repas de "fête"
:::

:::tip Récompenses

- Oeuf de vouivre de désert, utilisé directement pour obtenir des cartes de magie
- Graines de fleur de lune
- Coffre rempli d'or, richesses de Caldera (appartient en partie à Avelina et Xara, redevables)
- Avelina, ancienne bandite de Caldera
- Xara, ancienne bandite de Caldera
- Flora, la femme de Roget, est devenue la bourgmestre
- Le village de Claveau est libre... sous la direction de Rideau
:::

:::warning Personnages

- Gédéon noyé dans le Parcoul
- Julia, libre du joug de Caldera
- Caldera, tué par empoisonnement
- Corekciler, bandit de Caldera, ami d'Isilwen, tué par empoisonnement
- Ito, bandit de la bande de Caldera, homme d'arme, tué par empoisonnement
- Petitping, bandit de la bande de Caldera, homme d'arme, tué par empoisonnement
- Lixandro, bandit de la bande de Caldera, homme d'arme, tué par empoisonnement
- Avelina, ancienne bandite de Caldera, nouvelle membre du groupe des Sombres
- Xara, ancienne bandite de Caldera, nouvelle membre du groupe des Sombres
- Alvarro, bandit de la bande de Caldera, archer, tué par empoisonnement
- Ernoullet, aubergiste de la ville, anarqué par la bande de Caldera, cherche justice, tué par Yoni pour maquiller l'empoisonnement de la bande de Caldera
- Chevalier Garnier, victime du regard foudroyant d'Isilwen, chef de la troupe du seigneur Anne de Rideau
- Elias, notaire du grand marché d'Aria
- Philodias Plaza, agent immobilier d'Elias, a fait visiter deux maisons au groupe
:::

:::info Informations

- En Osmanlie, il y aurait des troubles avec le Khan
:::

## 3. Plaza Immobilier

:::info Joueuses
Isilwen, Pepa, Yoni
:::

**Le groupe est retourné à Aria pour acquérir des biens afin de placer leur or.**

C'est avec Philodas Plaza que nos héroïnes se décident pour la maison du quartier des plaisirs, une demeure de taille moyenne qui est un peu laissée à l'abandon mais qui possèd plusieurs chambres pour tout le groupe ainsi que d'une arrière cours avec une écurie à moitié en ruine. Ainsi Philodas emmène la bande voir Herménus Bordeleau, le noble dédaigneux possédant la maison qu'elles souhaitent acquérir, et suite à une négociation sévère de la part de Yoni, le prix est fixé à 200 couronnes. Le groupe se déplace donc jusque chez Elias, le notaire et patron de Philodas, pour finaliser la négociation, c'est là qu'Elias objecte qu'une partie de la fortune des aventurières n'a pas été expertisé et que la valeur de des richesses trouvée à Claveau, celles-ci sont soumises à questionnement. Il leur indique où trouver un prêteur sur gage qui est aussi son associé : Saleel Al Fara, c'est un groupe méfiant qui transporte sa fortune jusqu'à la boutique du marchand pour évaluation de leur fortune. Celui-ci, peu aimable mais professionnel, expertise l'ensemble à la hauteur de 110 couronnes alors que Yoni se promène dans sa boutique et découvre le *Codex Naturalia* qu'elle observe avec convoitise.

Les Sombres retournent donc voir Elias avec leur butin expertisé pour effectuer la transaction avec tout le contenu de la caisse provenant du manoir de Caldera en ajoutant des couronnes provenant de leurs richesses individuelles. Elles possèdent donc maintenant une maison dans le quartier des plaisirs, Elias leur indiquant que l'acte de propriété sera rédigé et leur sera transmis dans les plus brefs délais. Nos héroïnes se lancent dans l'exploration de leur nouvelle propriété et découvrent une cave accessible depuis la cuisine où se trouve un squelette dont la tête semble avoir été défoncée, elles trouveront le chevalière ornée d'un oriflamme d'une famille noble. La cave propose aussi un système expérimental pour gérer l'eau afin de la distribuer dans toute la maison, un assemblage curieux de tubes de cuivre qui est visiblement plutôt abimé mais qu'elles réparent à leur manière, ce qui permet à la cuisine d'avoir l'eau courante. L'étage propose des chambres assez vastes et une salle de bain très expérimentale où l'eau est apportée par le dispositif de la cave. Le soir même Yoni déclare qu'elle souhaite subtiliser le *Codex Naturalia* et c'est Yoni, Isilwen et Pépa qui se lancent dans un vol d'envergure qui se complique très vite et frôle la catastrophe. Mais finalement le groupe se trouve en possession du codex qui sera caché dans la maison. Le matin, nos héroïnes considèrent qu'il reste trop peu de meubles, ce qui les poussent à aller chercher de quoi remplir leur nouvelle maison.

Elles se rendent sur la place du marché pour finir par y trouver une vente aux enchères de meubles aqabiens d'un noble déchu. C'est lors d'une vente assez délicate qu'elles feront l'acquisition de plusieurs meubles de qualité dont assez de lits pour tout le monde, diminuant d'autant leurs richesses. Yoni se fera attaquer en plein rue par un homme mystérieux ayant deux parchemins sur lui qu'elles trouveront après l'avoir tué. Elles reviendront chez elles pour organiser l'intérieur et se répartir les meubles, le reste de la journée sera consacré à l'étude du *Codex Naturalia* et à leur défense lors de l'arrivée du capitaine de la garde qui enquête sur le vol chez Saleel Al Fara, c'est par la ruse qu'elles parviendront à le faire partir assez rapidement. Nos héroïnes découvriront que les parchemins que l'attaquant de la nuit précédente portait décrit la personne de Yoni en détails et que le deuxième est une carte sommaire allant de l'Osmanlie à Aria.

Le soir même, en constatant qu'elles sont assez mal en point, elles se rendent au temple d'Ina pour se faire soigner en échange de quelques pièces. C'est là qu'Arkhana leur parlera de Fripouille, un chat sacré d'Ina, qui semble s'évanouir dans la nature chaque nuit, ce qui est fort curieux pour un chat sacré qui reste d'habitude au temple. Intéressées, le groupe va se lancer dans la filature du chat qui traverse la moitié de la ville pour se rendre dans le ghetto d'Aqabah. Lors d'une tentative d'escalade Yoni continue à filer Fripouille alors qu'Isilwen traverse le toit d'une taverne locale en tombant. Elle s'assomme en tombant sur le comptoir alors que Pépa tente d'apaiser les choses avec un tavernier furieux qui appelle la garde. Pendant ce temps, Yoni arrive jusqu'à la maison où le chat se rendait, c'est là qu'elle va découvrir trois esclaves : Noura, Jamila et Malika alors que l'esclavage est interdit à Aria. Elle va les délivrer et s'échapper sans que le propriétaire ne les voit. Elle retrouve ses deux complices à la taverne en plein chaos et c'est grâce à son bagou légendaire et le jeu d'actrice de Pépa qu'elles s'échapperont avec une Isilwen à moitié inconsciente pour retourner au temple d'Ina. Le prête d'Ina sera ravi d'apprendre la vérité et de soigner Isilwen avant de leur offrir des récompenses très utiles comme une amélioration de la communication avec les animaux pour Pépa, une amélioration de compétence pour Yoni et l'amitié de Fripouille pour Isilwen.

Après cette aventure épique, elles se rendent à la colline creuse où des artisans proposent leurs productions, et elles achèteront encore quelques objets pour leur maison.

:::tip Récompenses

- Maison dans le quartier des plaisirs, négociée à 200 couronnes, au croisement de la rue de la *Vieille épée* et du *Fier taureau*
- Meubles récupérés aux enchères pour leur maison
- Yoni : un point de vue supplémentaire
- Isilwen : amour de Fripouille (nouveau familier)
- Pépa : Affinité avec les animeaux passe de 20% à 70% pour son faucon et 50% pour les autre
- *Codex Naturalia* (p. 144, tome I) lu par tous les membres du groupe
- Trois anciennes esclaves engagées pour leur maison : Noura, Jamila, Malika
- Chevalière des Santa Sofia
:::

:::warning Personnages

- Fripouille, chat sacré d'Ina, accompagne Isilwen
- Said Ben Said (non-rencontré), marchand d'esclaves non rencontré par le groupe mais dont les esclaves ont été libérées
- Elias, notaire du grand marché d'Aria
- Philodias Plaza, agent immobilier d'Elias, a fait visiter deux maisons au groupe
- Herménus Bordeleau, propriétaire de la maison dans le quartier des plaisirs
- Saleel Al Fara, prêteur sur gage faisant l'inventaire des richesses du groupe, le *Codex Naturalia* a été volé par Yoni dans sa boutique
- Noura, Jamila, Malika : esclaves de Said Ben Said
- Xara & Avelina, dans leur maison du quartier des plaisirs
- Arkhana, prêtre d'Ina
:::

:::info informations

- Un assassin osmanlien a tenté de tuer Yoni, il portait une description d'elle
:::

:::danger Pertes

- 200 couronnes pour leurs achats
:::

## 4. "Ça va heurter, chéri !"

:::info Joueuses
Isilwen, Pepa, Yoni
:::

**Suite à leur placement immobilier et leurs achats d'ammeublement, notre groupe continue ses aventures en ville avec la légendaire guilde des Poissonniers pour laquelle nos héroïnes vont devoir prouver leur valeur.**

Sur l'impulsion de Yoni, nos héroïnes vont tenter de trouver la guilde des Poissonniers afin d'en faire partie. Elles se rendront sur la place sur marché où elles vont errer pendant un certain temps avant de rencontrer Aristarque près d'une statue de dieu Poisson qui semble être un camelot. Elles vont tenter une approche assez directe qui déplaira à Aristarque mais il les enverra rencontrer Astrid aux deux visages qui se chargera de voir si elles sont assez douées pour rejoindre la guilde. Celle-ci se trouve à l'Arbalète, la meilleure taverne de tout Aria (et la plus chère) où elles trouveront l'agente de la guilde qui commandera une truite fumée en les faisant ostentatoirement patienter. Elle finira par leur confier une mission : voler un objet de valeur à des badauds sur la place proche de l'Arbalète. Notre groupe estimera plusieurs victimes possibles entre un émissaire aqabien fumant la pipe et une noble écoutant de la musique. Bien qu'un seul objet leur a été demandé, elles souhaiteront en ramener au moins deux : la pipe d'ivoire de l'émissaire aqabien et un bijou en émeraude de la noble. C'est par la ruse qu'elles réussiront leur forfait mais la noble, Marilisia Joncal, réagira très mal à leur approche et refusera de leur parler après cela alors que l'émissaire aqabien appelle la garde sans aucune preuve contre nos voleuses en herbe.

Elles ont ramené le résultat de leur vol auprès d'Astrid qui finissait sa truite fumée. Celle-ci ne se montre guère impressionnée mais accepte leur candidature, c'est là que Yoni demande à bénéficier de certains services de la guilde, en particulier pour obtenir des informations sur les Santa Sofia. Astrid se moquent d'elles en leur affirmant qu'elles n'ont pas le statut nécessaire pour accéder à de tels services. Cependant elle leur propose une mission plus délicate qui leur permettra de grimper les échelons de la guilde plus vite : faire boire le contenu d'une fiole à Klemmo Treherne, le plus riche noble de la ville, lors d'une fête privée qu'il donne le soir même, si possible par la discrétion et à 23h59.

Pour effectuer cette mission, elles étudieront plusieurs options depuis la place des nobles : Yoni tentera de conquérir Marilisia Joncal quand Isilwen et Pépa tenteront de gagner l'amitié d'un noble, Eliott Déziel, rencontré dans les jardins de la place. Leurs deux tentatives vont réussir plutôt bien lorsque Yoni fera un cadeau tendance (un roman à l'eau de rose) à Marilisia et une discussion très spirituelle avec Eliott. Après une négociation animée, Marilisia accepte de les aider à entrer à la fête qui nécessite une invitation. Elles se rendront à une boutique de fripier avec Eliott pour obtenir des déguisements ostentatoires répondant au thème de la soirée.

C'est durant cette soirée qu'elles apprendront plusieurs choses :

- Armory Treherne, le fils unique des Treherne, a disparu
- Le roi Blaise d'Aria est en mauvaise santé et que c'est sa fille la princesse Auriane gui s'occupe du royaume.
- Le prince Estienne est parti en croisade à Aqabah pour retrouver le Sceptre du Roi des rois, une des trois reliques avec l'Orbe et la Couronne

Elles vont profiter de la soirée en faisant connaissance avec plusieurs nobles, Pépa boira plus que de raison lors d'un concours et sera sauvée par ses deux compagnes qui la feront vomir pour la rendre sobre. Le repas est royal, proposant nombre de nourritures exotiques. Et c'est plus tard dans la soirée que Klemmo Treherne se montre et que par un discours incroyable nos héroïnes vont parvenir à attirer l'attention de Klemmo à tel point que celui-ci va leur proposer se voir sa collection de heurtoirs, sa passion.

Le groupe va arriver à rentrer dans le sanctuaire secret de Klemmo où celui-ci leur montre des heurtoirs exotiques venant des quatre coins du monde dont une curieux heurtoir en chitine d'araignée géante offert par un aventurier récemment. Elles continueront à discourir sur les heurtoirs et leur passion partagée pour ces objets à travers des histoires d'enfance qui mènera Klemmo à narrer sa propre histoire. Entre-temps, Pépa s'occupe de préparer les verres avec la bouteille de vin que Klemmo lui a confié alors qu'un domestique arrive en indiquant que la chancelière de la guilde des marchands va faire un discours officiel. Mais notre groupe est maintenant tellement proche de Klemmo que celui-ci renvoie le domestique en préférant rester avec de vrais amis. Et c'est à 23h59 qu'elles porteront un toast avec Klemmo en prenant soin de verser le contenu de la fiole dans son verre.

Elles observeront sa réaction alors que celui-ci continue à parler de sa passion et que rien ne se passe visiblement. Le groupe finira par prendre congé, en découvrant au passage que la chandelière en plein discours n'est autre qu'Astrid aux deux visages, qui les rejoindra deux heures plus tard dans la rue. Celle-ci reconnaitra leur valeur et acceptera de leur donner le rang de Compagne de la guilde des voleurs, ce qui leur permet d'accéder au QG de la guilde grâce à un passage par la statue de Gobi, le dieu Poisson. Elles s'y rendront en plongeant dans les souterrains d'Aria pour arriver jusqu'à Rupert le Requin, le chef de la guilde. Le groupe sera intronisé dans la guilde des voleurs et Yoni négociera des informations sur les Santa Sofia. Lorsque Yoni demande à voir un receleur, Rupert leur indique un membre de la guilde qui les conduira en ville, et c'est Jotun qui se dévoile en les regardant avec reproche.

Bien qu'il ait des raison de leur en vouloir, Jotun les accompgnera jusque chez Saleel Al Fara, également receleur pour la guilde. Celui-ci se montrera plus aimable avec le groupe et acceptera de discuter à propos d'objets exotiques : un collier et le livre des sorcières du Kohest. Puis, intriguée par l'histoire des Santa Sofia, les aventurières suivront les indices de Rupert et se rendront dans le quartier des Ombres où elles recroiseront Artemanorik, la vendeuse qu'elles avaient rencontré lors de leurs aventures à l'Académie d'Aria. Elle leur apprendra ce qu'elles ont besoin pour retrouver Zlatsko, l'ancien majordome des Santa Sofia, décédé à la pension des Merveilles. Yoni discutera encore à propos de la vente ou de la réparation de sa lampe éternelle qui ne brille qu'en plein soleil obtenue auprès de Shazam à Sannakale. Enfin le groupe arrivera jusqu'à la pension concernée, gérée par madame Dounia, une vieille tenancière qui tient ce cloaque qui propose également des services de maison de passe. Le groupe obtiendra les lettres que Zlatsko a laissé avant de mourrir en affirmant faire partie de la famille et en réglant ses dettes.

Ainsi le groupe comprendra qu'Atlan Santa Sofia, unique héritier, a disparu depuis des années mais que Zlatsko l'attendait et lui a laissé deux lettres dont une indiquant qu'il ne devait pas l'ouvrir. Elles ouvriront bien sûr la dernière lettre qui leur indiquera que le manoir Santa Sofia dispose d'un secret près de la statue dans le jardin du manoir. Pour s'y rendre, la première lettre fait référence à Ibolya, capitaine de *La Beauté circasienne*, un navire voyageant entre Aria et Aqabah, qui pourra aider Atlan.

Elles se rendront donc au port de commerce d'Aria pour trouver le navire mentionné par Zlatsko, et c'est César Costa, le maître des docks, qui leur indiquera l'emplacement du navire d'Ibloya. La capitaine, un peu rude, sera calmée très rapidement par nos héroïnes qui feront preuve d'une subtilité douteuse lors de leur rencontre. Elles indiqueront ainsi qu'elles tentent de retrouver Atlan pour l'aider et qu'elles viennent de la part de Zlatsko. La capitaine acceptera de les prendre sur leur navire si elles y travaillent car elle n'a plus d'équipage disponible. Avant de partir, elles retournent à leur maison du quartier des plaisirs pour régler les derniers détails en indiquant à Xana et Avelina qu'elles vont partir toutes les trois mais qu'elles peuvent rester à Aria. Les anciennes esclaves libérées affirment qu'elles souhaitent repartir à Aqabah et faire partie de l'équipage du navire, ce que nos aventurières acceptent un peu à contre-coeur. Le groupe retourne au port de commerce jusqu'à *La Beauté circasienne*.

:::warning Personnages

- Aristarque, un charlatan de la place du marché qui trouve de jeunes talents pour la guide des voleurs, toujours habillé de rouge
- Astrid aux deux visages, une membre notable de la guilde des voleurs qui donne deux contrats au groupe
- Un émissaire aqabien, dont la pipe en ivoire a été volée par le groupe
- Marilisia Joncal, noble qui a été volée par le groupe et que Yoni a tenter de séduire avec succès
- Eliott Déziel, noble rencontré dans le quartier des manoirs et qui devient un allié du groupe après quelques échanges
- Klemmo Treherne, chef de la famille Treherne, la plus riche d'Aria, collectionne les heurtoirs, il est devenu un très grand ami du groupe
- Rupert le Requin, chef de la guilde des voleurs
- Jotun, un membre loyal de la guilde des voleurs qui n'a pas été ravi de recroiser le groupe qui l'avait abandonné dans une fillette
- Saleel Al Fara, le prêteur sur gage se révèle être également receleur pour la guilde des voleurs
- Artemanorik, vendeuse du quartier des Ombres, elle dira au groupe ce qu'elle sait sur les Santa Sofia et sur Zlatsko. Porte des bijoux de verroterie, elle [a vendu la lampe mécanique à Shingui](/gamelog/aria/preludes#_2-une-sorciere-chez-les-mages). Apprécie bien le groupe
- Madame Dounia, gérante de la pensions des merveilles, un immonde cloaque qui fait aussi maison de passe
- Zlatsko (non-rencontré), ancien majordome de la famille Santa Sofia, a sans doute sauvé Atlan, le fils unique de la famille, et est mort seul à la pension des merveilles. Il a laissé des notes pour Atlan Santa Sofia dont le groupe s'empare
- César Costa, maître des docks, gérant de la taverne de l'Anguille d'argent, très réputée, à indiqué au groupe où trouver *La Beauté circasienne*
- Ibolya, capitaine du navire *La Beauté circasienne*. Elle aidera les membres du groupe après avoir appris qu'elles cherchaient Atlan
- Atlan Santa Sofia (non-rencontré), fils de la famille Santa Sofia, disparu et cherché par Zlatsko ainsi qu'Ibolya. Semble être lié à une prophétie et à l'ordre des Protecteurs
- Zoltan l'archiprêtre du Dieu Exilé (non-rencontré), n'a pas rencontré le groupe mais nos héroïnes savent qu'il a fait exterminer les Santa Sofia d'Aria
:::

:::tip Récompenses

- Membres de la guilde des Poissonniers, aka la guilde des voleurs au rang de Novices tout d'abord puis au rang de Compagnons après leur exploit chez Klemmo Treherne
- Un travail : engagées par Ibolya sur son navire *La Beauté circasienne*
- L'accès aux receleurs privés de la guilde
- L'amitié indéfectible de Klemmo Treherne
- L'amour naissant de Marilisia Joncal pour Yoni
- Les derniers mots de Zlatsko, en particulier sur Atlan Santa Sofia et l'ordre des Protecteurs
:::

:::info Informations

- Zoltan l'archiprêtre du Dieu Exilé a fait tuer la famille Santa Sofia, liée à l'ordre des Protecteurs
- Atlan Santa Sofia est porté disparu et recherché par Zlatsko et Ibolya
- Une statue du manoir familial des Santa Sofia semble cacher un secret
- Les esclaves libérées par le groupe souhaitent partir sur le navire *La Beauté circasienne* pour repartir chez elles, à Aqabah
- Amory Treherne, le fils unique des Treherne, a disparu
- Le roi Blaise d'Aria semble en mauvaise santé et c'est sa fille, la princesse Auriane d'Aria gère le royaume
- Le prince Estienne est parti en croisade à Aqabah pour récupérer le Sceptre d'Aria
- Trois reliques existent de part le monde : le Sceptre, l'Orbe et la Couronne du Roi des rois
:::

## 5. Next

- Dague perdue d'Atlan rendue par Saleel Al Fara (physiquement)
- Acte de propriété transmis par Saleel Al Fara (physiquement)
- Messages de Zlatsko (physiquement)
- Clé de la maison (physiquement)
