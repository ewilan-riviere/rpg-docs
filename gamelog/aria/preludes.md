# Préludes

Ces épisodes se sont produits avant la [saison 01](/gamelog/aria/saison-01).

## 1. Une nuit en Osmanlie

:::info Joueuses
Isilwen, Yoni
:::

Rencontre entre **Yoni** et **Isilwen** dans une taverne lors d'une attaque de la cité de **Varna** par l'arme osmalienne du **Khan**. C'est à ce moment que Yoni vole **Yakari**, un étalon pur-sang osmanlien. Nos héroïnes sont obligées de faire équipe suite à l'attaque et s'échappent par une des brèches dans le mur de Varna. Elles sont rapidement capturées par des soldats d'une seigneur locale, **Dame Dilara**, et accusées de braconnage.

Elles s'embarquent dans le sauvetage de **Senol**, la mère de Dame Dilara, suite à un chantage lié à un **chat sacré** de Varna capturé par Dame Dilara. La prisonnière est détenue au midi de l'Osmanlie, à **Sannakale** où le seigneur des pirates, le **Corbeau Noir**, règne en maître sur la cité séculaire.

Nos héroïnes parviennent à la ville en ruine et s'infiltrent de façon plus ou moins discrète en sauvant au passage une membre de la **garde des Renards**, une milice locale en guerre froide avec le Corbeau Noir. Cette soldat, Gergana, va les aider à approcher **Diego**, le bras droit du chef des pirates qui se trouve dans la taverne principale à côté de la tour de Sannakale où le Corbeau Noir détient Senol. Des informations leur indiquent également que la mère de Dame Dilara semble être là de son plein gré et aurait une romance avec le chef des pirates.

En parvenant dans la taverne la plus grande la ville, Isilwen et Yoni découvrent que Diego garde près de lui un prisonnier d'apparence curieuse qui ressemble à un magicien. Elles participent à la vie de la taverne, en particulier lorsqu'Isilwen bat un minotaure au bras de fer, ce qui leur permet d'approcher un Diego assez impressionné. C'est là qu'elles parlent avec le magicien qui indique s'appeler **Shazam**.

Après s'être concertées, elles décident de libérer Shazam qui leur offre un objet magique pour les récompenser : une *poêle avec un beig*net qui contrôle les pensées de la personne qui le mange. Yoni tentera d'arnaquer Shazam en négociant également une *lampe éternelle qui ne fonctionne qu'en plein soleil*, ce qu'elle réussira à obtenir.

Nos protagonistes vont longtemps hésiter avant de piéger Diego mais elles vont découvrir qu'il est secrètement amoureux de Senol et elles le piègeront pour lui faire manger le beignet afin de le faire entrer dans la tour où se trouve Senol. Avec le beignet magique, nos héroïnes vont contrôler les émotions de Diego afin de le faire grimper la tour à coup de ruse et de coups de poing. Il arrivera en haut de la tour et trouvera Senol avec le Corbeau Noir, endormis et dans leur plus simple appareil. Diego réussira, à travers nos héroïnes, à récupérer Senol et à la sortir de la tour. Elles verront Diego revenir à lui et courir tout raconter au seigneur des pirates qui le tuera directement suite à sa trahison.

Isilwen et Yoni vont s'emparer de Senol afin de la sortir de la ville en oubliant leur rendez-vous au port avec Dame Dilara, elles ramèneront la prisonnière au fief de la seigneur en attendant son retour. Quand la noble revient, elles sont couvertes de gloire et assistent à une scène peu banale où Senol tente une rebellion contre sa fille en affirmant son amour pour le chef des pirates.

Elles choisiront de ne pas intervenir et receveront de l'or, une jument, **Rebelle**, pour Isilwen et le chat sacré de Varna avant de repartir vers la cité frontalière. Une fois arrivées avec le chat sacré, elles seront reçues par la chef de la garde de Varna, **Zénaïse Garand**, et par le roi de Varna, **Enoïl Deschênes**, afin d'être grandement récompensées en or et avec la promesse de deux statues en hommage. Il faut noter que Yoni séduira Zénaïse Garand au passage.

:::tip Récompenses

- Yakari, étalon, pur-sang osmanlien volé à un soldat du Khan
- Rebelle, jument, croisée des écurie de Dame Dilara
- Beaucoup (trop) de couronnes
- Deux statues à Varna représentant les joueuses
  - Isilwen : marbre gris clair, visage découvert
  - Yoni : marbre veiné de bleu, chevelure rouge, drapé nue
- Lampe éternelle qui ne fonctionne qu'en plein soleil
:::

:::warning Personnages

- Dame Dilara, seigneur locale d'Osmanlie et alliée de nos héroïnes
- Gergana, soldat de la garde des Renards
- Diego, tué par le Corbeau Noir
- Corbeau Noir, a perdu sa promise et son bras droit, il n'a pas rencontré nos héroïnes
- Shazam, un mage excentrique
- Senol de retour chez Dame Dilara, prisonnière
- Alfétus Garand, capitaine de la garde de Varna (les Homme chats), séduite par Yoni
- Enoïl Deschênes, roi de Varna, allié de nos héroïnes
:::

## 2. Une sorcière chez les mages

:::info Joueuses
Isilwen, Pepa, Yoni
:::

### 1ère session

Nos deux précédentes héroïnes ont vécu quelques aventures comme mercenaires depuis Varna, elles croisent sur la route un vieil homme, **Adéliphus**, dans une carriole accompagné par une enfant. Cette dernière est en réalité **Pepa**, une Môn d'environ 16 ans et elle ressemble à une enfant humaine de 12 ans, elle sera la troisième héroïne de ce groupe. Le vieil homme indique être un mage itinérant qui rentre à Aria car il n'a pas trouvé d'apprenti et il a croisé Pepa sur la route qui souhaite voir la grande ville.

Alors que le groupe continue vers Aria, Adéliphus semble aller mal et propose au groupe de passer par sa demeure pour qu'il puisse prendre un remontant. Sauf qu'arrivés au manoir du mage, celui-ci s'écroule au sol, pris de spasmes. Notre groupe cherchera le filtre d'ellebore que le vieux mage leur a indiqué avant de sombrer dans une torpeur agitée, Pepa découvre au passage un "M" gravé dans la nuque du mage. Elles trouveront le filtre mais celui-ci ne pourra que stabiliser le mage et non le soigner. C'est le domestique d'Adéliphus qui leur indique de se rendre à l'académie des mages d'Aria pour y trouver un remède.

Sur le chemin, elles croiseront un enfant poursuivi par une **stryge** monstrueuse. Bien que bonne conductrice, Isilwen ne pourra éviter l'enfant qui passera sous les roues de la carriole et la stryge s'enfuiera après un combat rapide, Yoni découvrira un médaillon orné d'un "M" stylisé. Leur arrivée à l'académie ne passe pas inaperçue lorsque **Friklow**, l'intendant, les reçoit : entre un mage mourant et Linga qui s'envole vers une étrange et immense sphère bleutée en mouvement dans le hall de l'académie. Au contact de celle-ci et alors que Yoni s'est aggrippée à lui, le maléfice qui touche son frère se transfert à sa soeur et Linga redevient un instant humain jusqu'à ce que sa soeur le lache. C'est Friklow, débordé par tous ces évènements, qui va chercher l'**archimage Alphapizza** afin d'arrêter la sphère, libérer Linga et régler cette affaire de mage.

Nos protagonistes sont considérés comme des apprentis d'Adéliphus en encouragés à participer à la cérémonie du dragon, le lendemain. Elles acceptent d'entrer à l'académie et Isilwen se retrouve avec Yoni dans une chambre occupée par **Myrou**, une jeune noble prétentieuse alors que Pepa se retrouve avec **Gorniak**, un jeune étudiant réservé. Dans la nuit, un certain **Shingui**, élève de 3ème année, va leur faire une visite de bienvenue en les rassurant sur la cérémonie du lendemain.

Au petit matin, notre groupe est réuni avec d'autres élèves dans le jardin central pour absorber un morceau de foetus de vouivre du désert qui confère des pouvoirs magiques. Nos héroïnes ne s'en sortent pas très bien mais parviennent à obtenir des pouvoirs magiques mineurs. Les nouveaux venus de l'académie sont confiés à **Hercélie**, une 3ème année, peu sympathique qui les fera courir dans toute l'académie pour résoudre un mystère. Notre groupe résoudra l'enigme s'accordant au passage la haine d'Hercélie qui est ridiculisée.

La nuit même, un fracas de verre suivi d'un choc sourd réveille un bon nombre d'élèves qui se réunissent dans le jardin central pour découvrir le corps sans vie de Shingui qui semble avoir été jeté de sa fenêtre. Alors que le corps enseignant calme les émotions et prétend à un suicide, nos héroïnes vont enquêter une partie de la nuit et tout le reste du lendemain. Elles vont explorer la chambre de Shingui et y trouver des plumes, interroger **Grandpapa**, son collocataire, qui leur avoue être amoureux de lui, filer **Connikil** (un rival de Shingui) qui se fait larguer par Hercélie elle-même avant de découvrir que celle-ci est également amoureuse de Shingui. Un triangle amoureux qui a tourné mal ? Au passage Yoni entamera une relation avec la prêtresse d'Ina, Doriana, qui se trouve à l'infirmerie de l'académie.

### 2ème session

L'enquête continue dans les sous-sols de l'académie où le groupe découvre un passage secret qui les mène dans une partie oubliée des égoûts d'Aria. Leurs périgrinations humides les mèneront à une antre perdue dans le labyrinthe de canaux souterrains où le groupe découvrira deux **sorcières du Kohest** menaçant d'exécuter Hercélie qui est attachée à une table de pierre ancienne. Les sorcière seront vaincues, torturées puis anéanties par nos héroïnes aux valeurs douteuses. Elles libéreront Hercélie qui avouera être une sorcière du Kohest qui a renoncé à ses valeurs pour Shingui dont elle était amoureuse. Pour les remercier, l'ancienne sorcière du Kohest ouvrira une cache secrète dont la porte de pierre est ornée d'un "M" qui représente en réalité deux montagnes. Le groupe y découvrira un **livre enchaîné** à un pupitre.

Le groupe est félicité par l'archimage lorsque celui-ci apprend tout ce qu'il s'est passé et leur remet des couronnes et les déclare "Mages honoraires de l'académie d'Aria" pour service rendu. Yoni apprend que seul le mage légendaire Tamerlan pourrait libérer Linga de sa situation animale, ou alors le mage à l'origine du problème. Elle refuse de laisser Linga étudier à l'académie malgré ses prédispositions magiques détectées par Alphapizza.

Isilwen entamera une étude approfondie sur les elfes noirs albinos, connus sous le nom Isilnesaë (Soeurs de la Lune), avec le professeur **Junko** et ils découvriront qu'Isilwen a une affinité naturelle avec la magie de la mort. Junko indique à Isilwen qu'il a bien connu une autre elfe noire comme elle mais que celle-ci est morte des sévices qu'elle avait subi auprès de personnes cruelles. Leur collaboration s'est arrêtée pour l'instant mais Isilwen possède désormais plus de connaissances sur elle-même. Pepa s'est vue remerciée personnellement par le professeur Gabier pour le soutien qu'elle a apporté à l'enquête en se voyant offrir un faucon pélerin qu'elle nommera Thor, avec une capacité à communiquer avec lui.

Le groupe finira ses études durant un mois entier avant de quitter l'académie et de s'engager comme mercenaires, c'est après avoir mené la chasse à des bandits que le groupe découvre le corps d'un homme pendu sur lequel Yoni trouvera un parchemin indiquant qu'il s'agit du bailli itinérant Ordo. Le groupe ramènera le corps du bailli et les têtes des bandits, le préposé aux primes Tarcil Verney leur demandera d'apposer leur nom de mercenaire sur la prime, elles choisiront alors le nom "Les Sombres". Avant de se voir refuser l'intégralité de la prime affichée pour cause d'un bandit qui manquerait. C'est ainsi que Les Sombres repartirent en chasse.

:::tip Récompenses

- Des couronnes
- Un titre : "Mage honoraire de l'académie d'Aria"
- Thor, un faucon pélerin pour Pépa et la capacité de communiquer avec l'oiseau
- La magie de la mort pour Isilwen
- Le parchemin et la broche d'Ordo
- Un nom d'équipe légendaire, Les Sombres
- Le livre des sorcières
:::

:::warning Personnages

- Adéliphus, vieux mage assassiné par les sorcières du Kohest
- Sorcières du Kohest, sorcières aux compétences méconnues mais qui peuvent se métamorphoser (plusieurs clans existent)
- Alphapizza, archimage de l'académie
- Friklow, intendant de l'académie
- Gabier, professeur de la volière, a offert un faucon pélerin à Pepa
- Junko, professeur médecin, légiste, a collaboré avec Isilwen pour lui apprendre la magie de la mort, passioné par les Isilnesaë
- Myrou, une jeune noble prétentieuse, dans la chambre d'Isilwen et Yoni
- Gorniak, un jeune noble assez réservén, dans la chambre de Pépa
- Shingui, élève de 3ème année, assassiné par des sorcières du Kohest
- Hercélie, une 3ème année, amoureuse de Shingui, ancienne sorcière du Kohest
- Doriana, prêtresse d'Ina, amante de Yoni, travaille à l'infirmerie de l'académie
- Grandpapa, collocataire de Shingui, secrètement amoureux de lui
- Connikil, rival de Shingui, rejeté par Hercélie
- Ordo, bailli tué par des bandits
- Tarcil Verney, préposé du bureau des primes d'Aria
:::

:::info Informations

- Linga peut être retransformé en humain par un mage très puissant tel que Tamerlan, le mage mythique ou par le mage à l'origine de la transformation
:::
