# Joueur·ses

## Isilwen

![Isilwen](./images/isilwen.webp)

:::info Personnage

- Archétype : Aventurière
- Espèce : Elfe noire
- Métier : Chasseusse de prime
- Compétence spéciale : "Regard foudroyant", mettre la pression sur l'être vivant concerné
  - 50%, 1 fois/session, maximum 2 fois/être vivant
- Caractère : valeurs morales fortes, radicale
- Extra :
  - Elfe noire très pâle, considérée comme albinos
:::

:::tip Modifications

- Pratique la magie de la mort (35%)
- A une jument croisée des écuries de Dame Dilara (Osmanlie), Rebelle
- Possède un nombre de carte magiques moyen
:::

## Pepa

:::info Personnage

- Archétype : Alchimiste
- Espèce : Mônne
- Métier : ?
- Compétence spéciale : Alchismite, peut faire des potions
- Caractère : très curieuse, un peu naïve
- Extra :
  - Mônne issue d'une union entre une Mônne et un Homme blanc
:::

:::tip Modifications

- A un faucon pélerin, Thor et une compétence pour tenter de communiquer avec lui (70%)
- Communiquer avec les animaux à 50%
- ~~A un cheval, Hermano des écuries de Dame Hermance (Arance)~~ : tombé dans le Parcoul à Claveau
- Possèdes quelques cartes de magie très puissante
- Potions : feu ardent d’Ingramus le sage, passe-muraille de Karloff, potion de chance
:::

## Yoni

![Yoni](./images/yoni.webp)

:::info Personnage

- Archétype : Voleuse
- Espèce : Lynassi
- Métier : Marchande
- Compétence spéciale : "Manipulation outrageuse", obtenir presque n'importe quoi par son charme (limité à un être vivant pouvant communiquer)
  - 50%, 1 fois/session, maximum 2 fois/être vivant
- Caractère : très charmeuse, un peu égocentrique
- Extra :
  - Lynassi avec une couleur de peau plus claire que la moyenne
  - Dispose d'un rat qui semble très apprivoisé qui est en réalité son frère, Linga, transformé en rat après un vol qui a mal tourné. Il est doué pour la magie.
:::

:::tip Modifications

- A un étalon pur-sang osmanlien, Yakari volé à un soldat du Khan à Varna
- Possède un nombre de carte magiques moyen
:::

### Linga

:::info Personnage

- Archétype : Disciple étranger
- Espèce : Lynassi/Rat
- Métier : Marchand
- Compétence spéciale : Magicien (non-formé)
- Caractère : ?
:::

:::tip Modifications

- Possède un nombre de carte magiques moyen
:::
