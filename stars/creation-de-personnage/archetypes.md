---
title: Archétypes
---

# Archétypes

Voici une liste non exhaustive des archétypes que vous pouvez choisir :

## Pilote

Spécialités : *courir, sauter, nager*, *déplacement en zéro G*, *Pilotage*, *Réflexes* avec une *Dextérité*, une *Intelligence* et une *Intuition* élevées

Pilote de chasse de l'US Air Force, spécialiste des prototypes, vous êtes sans peur, limite suicidaire, toujours prêt·e à relever un défi. Vous êtes devenu·e pilote de navette spatiale à 35 ans et vous avsez déjà fait deux expéditions et deux arrimages auprès de l'ISS.

- vous avez des problèmes avec les jeux d'argent
- vous posez des problèmes à votre hiérarchie
- vous êtes le·a meilleur·e
- vous voulez l'aventure

## Astrophysicien·ne

Spécialités : *astrophysique, mathématiques*, *observer* avec une *Intelligence* très élevée

Vous êtes un·e génie de 28 ans, et ceux qui comprennent l'étendue de votre intelligence sont parfois terrifiés, souvent fascinés. Détenteur·ice d'une médaille de Navier-Strokes, vous avez également obtenu un prix Nobel pour une méthode qui porte votre nom et qui permet de déduire la structure d'un corps céleste en fonction de son rayonnement.

- vous avez votre propre bibliothèque où vous passez tout votre temps
- vous voulez trouver une solution pour dépasser la vitesse de la lumière
- Très solitaire, vos collègues vous considèrent comme dépressif·ve, vous n'avez pas de famille en-dehors de votre mère et pas d'amis

## Xénobiologiste

Spécialités : *Adaptation*, *Biologie, géologie*, *Comprendre l'étrange*, *Convaincre*, *Garder son calme* avec une bonne *Intelligence* et une très bonne *Intuition*

Vous avez un doctorat en linguistique, en philosophie et en biologie, vous avez aidé à la conception de Google Translate. Pourtant, le·e professionnel·le très respecté·e que vous êtes, à 38 ans, est souvent contesté·e pour vos prises de position sur les extraterrestres.

- vous croyez en l'existence des "anciens astronautes" et donc en les extraterrestres
- vous avez mauvaise réputation
- vous participez à SETI

## Ingénieur

Spécialités : *Adaptation*, *Bricoler, bidouiller*, *Observer* avec une *Dextérité* et *Endurance* bonnes

Vous avez conçu la statioon Mars First de SpaceY, au-delà de vos capacités d'architecte vous êtes capable de réparer n'importe quoi dans l'urgence avec ce que vous avez sous la main. La plupart des décollages réussis ces trois dernières années sont liés d'une manière ou d'une autre à vous. De fait, vous vous entendez très bien avec les machines informatiques.

- sympathique et apprécié·e
- très croyant (religion à choisir) et proche de sa famille
- souhaite aller dans l'espace avant sa retraite
