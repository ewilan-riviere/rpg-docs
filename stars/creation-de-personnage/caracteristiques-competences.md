# Caractéristiques et compétences

## Les caractéristiques principales

Un personnage dispose de cinq caractéristiques :

- La **Force** est une mesure de la puissance musculaire du personnage. Cette caractéristique donne une idée de ce qu’il peut soulever, porter, pousser ou tirer.
- La **Dextérité** mesure la vivacité, l’agilité et la rapidité physique du personnage.
- L’**Endurance** rend compte de la vitalité et de la santé du personnage.
- L’**Intelligence** décrit les capacités d’apprentissage, de mémorisation et d’analyse de votre personnage. Elle ne remplace pas l’intelligence du joueur, mais gère, à sa place, un certain nombre de tâches fastidieuses.
- L'**Intuition** décrit la capacité à savoir ce qu'on ne sait pas, comprendre l'incompréhensible et plus généralement à faire preuve d'intuition.

### Répartition

Répartissez **60 points** dans ces 5 caractéristiques avec un *minimum de 4* et un *maximum de 18*.
Une fois les caractéristiques définies, remplissez **les points de vie**, ils sont égaux à *l’Endurance avec un maximum de 14* et la **Santé mentale** qui est égale à l'Intuition avec un maximum de 14.

Au vu de ces statistiques, réfléchissez avec le joueur à la « profession » de son aventurier. *Soldat* ? *Érudit* ? *Marchand* ? *Diplomate* ?

## Les compétences

L’essentiel des actions pouvant échouer des personnages passe par le recours à des compétences. Chacune d’elles s’exprime en pourcentage et suppose de lancer un dé à cent faces (abrégé en 1d100). Si le résultat du dé est inférieur ou égal à la compétence, l’action est réussie.

> *Exemple* : Atlan tente de convaincre un noble de le laisser entrer à une soirée. Atlan a 80 % en Mentir, convaincre et son joueur lance deux dés. Avec un score de 59, Atlan parvient à convaincre le noble de le laisser passer.

### Renseigner ses compétences

Ce sont vos compétences qui vous permetteront de surmonter les épreuves que votre MJ préféré·e mettra sur votre chemin. Vous allez devoir renseigner vos capacités pour personnaliser son héros en développant davantage certaines compétences.

- Pour chaque compétence, ajoutez les deux caractéristiques liées et multipliez le tout par 2. Il s’agit du scoreinitial de la compétence.
- Une fois tous les scores initiaux renseignés, vous disposez de 50 points à répartir comme bon vous semble, sur la base de 1 pour 1. À la création, aucun personnage ne peut disposer d’une compétence supérieure à 90 %.

> *Exemple* : Atlan dispose toujours d’un score de 13 en Charisme et de 10 en Intelligence. Son score initial en Mentir, convaincre est donc de: (13+10) x 2, soit 46 %. Il décide de dépenser 34 de ses 50 points pour améliorer cette compétence qu’il estime vitale, passant son score à 80 %.

### Liste des compétences

Les noms des compétences mentionnent souvent plusieurs domaines. Cette liste n’est pas exhaustive, mais vise à donner une idée de tout ce qui peut entrer sous l’intitulé de la compétence en question. Ainsi, « Courir, nager, sauter » recouvre n’importe quelle activité physique qui réclame un peu de technique et ne fait pas appel à la force brute ou à la dextérité naturelle. La majorité des sports entrent dans le champ d’application de cette compétence, par exemple.

**Adaptation** (Endurance, Intuition) : versant intellectuel de la compétence Survie, Adaptation permet de deviner si une plante est comestible, une atmosphère respirable, un liquide potable, et ce sans disposer du matériel d’analyse nécessaire, et plus généralement de deviner des choses à partir d’informations qui ne sont pas consciemment connues... Un peu comme un sixième sens ou une capacité psychique inconnue (mais la science a prouvé que cela n’existait pas, n’estce pas ?).

**Astrophysique, mathématiques** (Intelligence, Intuition) : mesure la compétence du personnage dans les sciences dures, ce qui inclue non seulement les deux mentionnées ici, mais aussi la géométrie, la physique, etc.

**Biologie, géologie** (Dextérité, Intelligence) : mesure la compétence du personnage dans les sciences de la vie et les sciences appliquées, à la fois sur le plan intellectuel (l’étendue de ses connaissances) mais aussi pratique (sa capacité à utiliser des équipements de laboratoire, à créer des produits chimiques, etc.).

**Bricoler**, bidouiller (Dextérité, Intuition) : permet de réparer, d’améliorer et de tripatouiller les dispositifs mécaniques ou électromécaniques, comme des moteurs, des générateurs...

**Combat à distance** (Dextérité, Intelligence) : permet d’utiliser des armes à distance, des plus primitives (javelot, arc, pierre lancée...) aux plus avancées (fusil, pistolet ou même des armes alien à distance). Un test de Comprendre l’étrange peut être requis avant d’employer les armements les plus exotiques.

**Combat au corps à corps** (Endurance, Force) : permet d’utiliser les armes de corps à corps, des plus primitives (pied, poing, couteau, massue, pierreutilisée pour frapper...) aux plus avancées (baïonnette, tronçonneuse ou même des armes alien de contact). Un test de Comprendre l’étrange peut être requis avant d’employer les armements lesplus exotiques. La compétence peut aussi servir pour parer une attaque, à supposer que les armes employées par l’attaquant et par le défenseur le permettent (un couteau alien qui tranche tout ce qu’il touche peut difficilement être paré et une tronçonneuse n’est définitivement pas une arme avec laquelle parer).

**Comprendre l’étrange** (Intelligence, Intuition) : capacité à comprendre tout ce qui relève des aliens. Cela peut aussi bien concerner les aliens euxmêmes (leurs rituels, leurs attitudes, leurs tabous...) que leurs objets ou leurs constructions.

**Convaincre** (Force, Intelligence) : recouvre à la fois l’art de l’argumentation et la capacité à se faire obéir, de gré ou de force. La démonstration, l’intimidation, le charme, la menace... Tout cela relève de Convaincre.

**Courir, sauter, nager** (Dextérité, Endurance) : cette compétence est employée pour toute activité physique qui requiert un minimum de technique. La majorité des sports relèvent de cette compétence, par exemple. En revanche, soulever quelque chose de lourd ou rattraper un objet qui tombe ne relèvent pas de cette compétence. Dans le premier cas, c’est une pure question **de** Force, dans le deuxième de Dextérité. Déplacement en zéro G (Dextérité, Force) : recouvre l’art de se déplacer en apesanteur. Le personnage sait gérer les poussées et les directions afin de se rendre là où il veut, sans manquer sa cible, s’écraser dessus ou la rejoindre à une allure d’escargot.

**Discrétion** (Dextérité, Intuition) : utilisée lorsque vous devez vous faire oublier, vous cacher, ne pas vous faire remarquer. Dans S.T.A.R.S, cette compétence ne permet pas de se déguiser, car, quel que soit le matériel disponible, se faire passer pour un alien est irréalisable. Dans le cadre d’une filature ou d’une poursuite, la compétence Discrétion du suiveur s’oppose à la compétence Observer du suivi.

**Garder son calme** (Endurance, Intuition) : l’art de résister à toutes les situations mentalement éprouvantes. Cela recouvre la capacité à résister à la torture, mais aussi celle de supporter des spectacles affreux sans en faire des cauchemars ou encore de tolérer le contact mental avec un esprit fondamentalement étranger, dans le cas d’un contact télépathique, par exemple. C’est la compétence qui mesure à quel point le personnage est mentalement stable et équilibré... ou susceptible de sombrer facilement dans la folie. Si le PJ peut, lors de ce test, toucher un objet fétiche qu’il a en sa possession, il obtient un bonus de +10 %. S’il peut même l’utiliser lors de ce test, le bonus est de +25 %.

**Observer** (Endurance, Intuition) : percevoir tout ce qui n’est pas évident grâce à ses sens. Attention, certains aliens disposent de plus de cinq sens et d’autres moins... Ou n’ont pas ceux auxquels les Hommes sont habitués.

**Pilotage** (Dextérité, Intelligence) : diriger n’importe quoi capable de se déplacer par luimême,depuis les montures animales jusqu’aux navires spatiaux, en passant par les voitures, les motos, les engins antigravité et autres bizarreries aliens. Un test de Comprendre l’étrange peut être requis avant d’employer les montures ou les machines les plus exotiques.

**Programmation** (Intelligence, Intuition) : mélange d’électronique et de connaissance des ordinateurs (les langages, le fonctionnement, etc.), c’est la compétence qui permet de faire fonctionner tout ce qui relève des systèmes informatiques, même ceux des aliens... Enfin, ceux qu’on parvient à comprendre.

**Réflexes** (Dextérité, Force) : parfois, vous devrez être vif pour échapper à un éboulement, empêcher un monstre alien de vous arracher le pied ou retenir quelqu’un qui vient de trébucher. Réflexes intervient précisément à ce momentlà. Cette compétence intervient aussi lors des combats pour déterminer qui agit en premier.

**Soigner** (Dextérité, Intelligence) : Capacité à diagnostiquer et à soigner. Efficace sur les Humains, moins sur les animaux de la Terre et souvent contreproductive sur les aliens. Un test de Soigner peut rendre un Point de Vie, une fois par jour.

**Survie** (Endurance, Intuition) : capacité à fabriquer ou obtenir de quoi survivre dans la nature. Faire un feu, trouver un abri, filtrer de l’eau, etc.

## Compétence spéciale

Chaque personnage dispose d'une compétence spéciale et selon la puissance de la compétence, le·a MJ définira la limite :

- Fréquence, exemple: une seule fois par jour
- Succès, exemple: 50% avec des conséquences en cas d'échec
- Utilisations, exemple: 3 utilisations maximales

Certaines quêtes telles que les quêtes personnelles peuvent donner des capacités spéciales supplémentaires.

## Objet fétiche

Pour vous aider lors de votre voyage dans les profondeurs de l'espace vous emportez un objet fétiche qui que vous pourrez utiliser pour vous rassurer.
