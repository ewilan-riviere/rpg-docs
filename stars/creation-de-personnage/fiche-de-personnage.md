---
title: Fiche de personnage
---

# Fiche de personnage

Essentielle dans votre aventure, cette fiche vous accompagnera tout au long de la campagne. Certaines zones ne sont pas liées à vos caractéristiques mais vous permettre de personnaliser votre héros. Le nom bien sûr, mais également son âge et sa description. Prenez un peu de temps pour songer à ces éléments et, ainsi, faire de votre personnage un être unique. Vous n’avez pas besoin d’en faire tout un roman, un ou deux traits caractéristiques suffiront (couleur des cheveux ou des yeux, corpulence, cicatrice, type de vêtements, attitude physique...).

<a href="./images/character.jpg" target="_blank" download="player">Télécharger</a>

![character](./images/character.jpg)

## Quand j’étais petit, je faisais

Une fois les compétences définies, le joueur peut demander à les augmenter. Il doit pour cela fournir une explication raisonnable au MJ, telle que « *Dans mon enfance, je devais échapper à des plus grands qui me martyrisaient, donc j’ai appris à courir vite!* ». Le meneur pourra alors autoriser d’augmenter la compétence de 10 points (voire 20 en cas de justification extrêmement pertinente).

Le MJ est invité à encourager ces demandes car elles remplissent l’histoire du joueur. Cependant, à chaque bonus accordé, il appliquera un malus équivalent à une autre compétence déjà acquise, en le justifiant. Par exemple « *Comme tu as passé ton enfance à fuir, tu restais solitaire et tu n’as jamais appris à négocier ou mentir dans des situations stressantes.* »

> Remarque : Vu le faible nombre de compétences disponibles, nous vous conseillons de limiter ces changements à trois maximum (trois bonus et trois malus).

## Phrase de synthèse

Nous avons vu plus haut que votre personnage devait pouvoir se résumer à la phrase « *Il est génial parce que... mais la société a des problèmes avec lui parce que...* ».

Maintenant que vous en savez plus sur ses données chiffrées, ses talents, voire son passé, essayez de compléter cette phrase de synthèse. L’important est qu’elle fournisse une motivation suffisamment forte à votre héros pour qu’il ait décidé de partir à l’aventure plutôt que de rester chez lui.

Notez que vous pouvez tout à fait conserver cette phrase de synthèse pour vous et votre MJ sans forcément en référer aux autres joueurs. Après tout, chaque personnage a ses petits secrets. Si vous êtes d’humeur joueuse, vous pouvez même vous inventer une phrase de synthèse «publique », connue des autres personnages, mais qui est en réalité très différente de la réalité de votre personnage. La littérature regorge de héros qui mentent sur leur origine au commencement de l’aventure, alors pourquoi pas vous ?
