# STARS

En 2018, la mission sur mars du rover Curiosity touche à sa fin suite à la perte de contact avec ce dernier. Le rover a cela dit bien rempli sa mission et l'équipe se démantèle petit à petit. Cependant, en août 2018, les ingénieurs de la NASA reçoivent à nouveau un signal du rover qui semble s'être remis en marche. Responsables de la mission restants, vous retournez dans les locaux de la NASA pour en apprendre plus. Vous découvrez alors qu'ils ont à nouveau le contrôle du rover, bien qu'ayant à leur disposition une quantité très limitée d'énergie.

Cela suffit cependant pour découvrir quelque chose d'incroyable : vous prennez en photo une structure triangulaire, clairement non naturelle mais qui ne ressemble également pas à quelque chose créé par l'être humain. Cette découverte relance l'intérêt pour Mars et une mission de grande envergure se prépare afin de vous envoyer sur Mars et peut-être rencontrer une vie extra-terrestre.
