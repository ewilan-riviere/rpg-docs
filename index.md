---
layout: home
sidebar: false

title: LesSombres™
titleTemplate: Documentation de jeux de rôles

hero:
  name: LesSombres™
  text: Documentation de jeux de rôles
  tagline: Regroupement d'informations sur les jeux de rôles joués par l'équipe de LesSombres™
  image:
    src: /logo-shadow.svg
    alt: LesSombres™
  actions:
    - theme: brand
      text: Campagne Aria
      link: /aria/
    - theme: alt
      text: Campagne STARS
      link: /stars/
    - theme: alt
      text: GameLog
      link: /gamelog/

features:
  - title: Game of Roles
    details: Basé sur les campagnes menées dans l'émission Game of Roles.
  - title: Pour les débutant·es
    details: Des règles simples et faciles à appréhender pour débuter facilement.
  - title: Dédié au fun
    details: Loin des règles rigides, les campagnes proposées visent à faire des sessions amusantes.
  - title: À la demande
    details: Le·a MJ proposera et les joueur·euses disposeront, entre liberté totale et quêtes fondamentales
---
