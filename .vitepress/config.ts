import { defineConfig } from 'vitepress'
import { version } from '../package.json'
import {
  font,
  gitlab,
  ogImage,
  ogUrl,
  gameOfRolesWiki,
  elderCraft,
  vitestDescription,
  vitestName,
} from './meta'
import { teamMembers } from './contributors'

export default defineConfig({
  lang: 'en-US',
  title: vitestName,
  description: vitestDescription,
  head: [
    ['meta', { name: 'theme-color', content: '#ffffff' }],
    ['link', { rel: 'icon', href: '/logo.svg', type: 'image/svg+xml' }],
    [
      'link',
      {
        rel: 'alternate icon',
        href: '/favicon.ico',
        type: 'image/png',
        sizes: '16x16',
      },
    ],
    [
      'meta',
      {
        name: 'author',
        content: `${teamMembers
          .map((c) => c.name)
          .join(', ')} and ${vitestName} contributors`,
      },
    ],
    [
      'meta',
      {
        name: 'keywords',
        content:
          'vitest, vite, test, coverage, snapshot, react, vue, preact, svelte, solid, lit, ruby, cypress, puppeteer, jsdom, happy-dom, test-runner, jest, typescript, esm, tinypool, tinyspy, c8, node',
      },
    ],
    ['meta', { property: 'og:title', content: vitestName }],
    ['meta', { property: 'og:description', content: vitestDescription }],
    ['meta', { property: 'og:url', content: ogUrl }],
    ['meta', { property: 'og:image', content: ogImage }],
    ['meta', { name: 'twitter:title', content: vitestName }],
    ['meta', { name: 'twitter:description', content: vitestDescription }],
    ['meta', { name: 'twitter:image', content: ogImage }],
    ['meta', { name: 'twitter:card', content: 'summary_large_image' }],
    ['link', { href: font, rel: 'stylesheet' }],
    ['link', { rel: 'mask-icon', href: '/logo.svg', color: '#ffffff' }],
    [
      'link',
      {
        rel: 'apple-touch-icon',
        href: '/apple-touch-icon.png',
        sizes: '180x180',
      },
    ],
  ],
  lastUpdated: true,
  markdown: {
    theme: {
      light: 'vitesse-light',
      dark: 'vitesse-dark',
    },
    toc: {
      includeLevel: [2, 3, 4],
    }
  },
  themeConfig: {
    logo: '/logo.svg',

    editLink: {
      repo: 'vitest-dev/vitest',
      branch: 'main',
      dir: 'docs',
      text: 'Suggest changes to this page',
    },

    // algolia: {
    //   appId: 'ZTF29HGJ69',
    //   apiKey: '9c3ced6fed60d2670bb36ab7e8bed8bc',
    //   indexName: 'vitest',
    //   // searchParameters: {
    //   //   facetFilters: ['tags:en'],
    //   // },
    // },

    // localeLinks: {
    //   text: 'English',
    //   items: [{ text: '简体中文', link: 'https://cn.vitest.dev' }],
    // },

    socialLinks: [
      // { icon: 'twitter', link: twitter },
      // { icon: 'discord', link: discord },
      { icon: 'github', link: gitlab },
    ],

    footer: {
      message: 'Released under the MIT License.',
      copyright:
        'Based on Vitest theme',
    },

    nav: [
      { text: 'Aria', link: '/aria/' },
      {
        text: 'STARS',
        activeMatch: `^/stars/`,
        link: '/stars/'
      },
      { text: 'GameLog', link: '/gamelog/' },
      {
        text: 'Liens',
        items: [
          {
            text: 'Game of Roles Wiki ',
            link: gameOfRolesWiki,
          },
          {
            text: 'Elder Craft ',
            link: elderCraft,
          },
        ],
      },
    ],

    sidebar: {
      '/aria/': [
        {
          text: 'Introduction',
          items: [
            {
              text: 'Notes',
              link: '/aria/notes'
            }
          ]
        },
        {
          text: 'Création de personnage',
          items: [
            {
              text: 'Apparence',
              link: '/aria/creation-de-personnage/apparence'
            },
            {
              text: 'Archétypes',
              link: '/aria/creation-de-personnage/archetypes'
            },
            {
              text: 'Caractéristiques et compétences',
              link: '/aria/creation-de-personnage/caracteristiques-competences'
            },
            {
              text: 'Equipement',
              link: '/aria/creation-de-personnage/equipement'
            },
            {
              text: 'Fiche de personnage',
              link: '/aria/creation-de-personnage/fiche-de-personnage'
            },
            {
              text: 'Historique',
              link: '/aria/creation-de-personnage/historique'
            }
          ]
        },
        {
          text: 'Magie',
          collapsible: true,
          collapsed: true,
          items: [
            {
              text: 'Principes',
              link: '/aria/magie/index'
            },
            {
              text: 'Noble magie',
              link: '/aria/magie/noble-magie'
            },
            {
              text: 'Alchimie et automates',
              link: '/aria/magie/alchimie-et-automates'
            },
            {
              text: 'Magie de la mort',
              link: '/aria/magie/magie-de-la-mort'
            },
          ]
        },
        {
          text: "Monde d'Aria",
          items: [
            {
              text: 'Pays principaux',
              link: '/aria/monde-d-aria/pays-principaux'
            },
            {
              text: 'Races jouables',
              link: '/aria/monde-d-aria/races-jouables'
            }
          ]
        }
      ],
      '/stars/': [
        {
          text: 'Creation de personnage',
          items: [
            {
              text: 'Archétypes',
              link: '/stars/creation-de-personnage/archetypes'
            },
            {
              text: 'Caractéristiques et compétences',
              link: '/stars/creation-de-personnage/caracteristiques-competences'
            },
            {
              text: 'Fiche de personnage',
              link: '/stars/creation-de-personnage/fiche-de-personnage'
            }
          ]
        },
      ],
      'gamelog': [
        // {
        //   text: 'Joueur·euses',
        //   // link: '/gamelog/joueureuses'
        // },
        {
          text: 'Aria',
          items: [
            {
              text: 'Joueur·euses',
              link: '/gamelog/aria/joueureuses'
            },
            {
              text: 'Préludes',
              link: '/gamelog/aria/preludes'
            },
            {
              text: 'Saison 01',
              link: '/gamelog/aria/saison-01'
            }
          ]
        }
      ]
    },
  },
})
