// noinspection ES6PreferShortImport: IntelliJ IDE hint to avoid warning to use `~/contributors`, will fail on build if changed

/* Texts */
export const vitestName = `LesSombres™`
export const vitestShortName = `LesSombres™`
export const vitestDescription = "Une documentation sur les jeux de rôles joués par l'équipe de LesSombres™"

/* CDN fonts and styles */
export const googleapis = 'https://fonts.googleapis.com'
export const gstatic = 'https://fonts.gstatic.com'
export const font = `${googleapis}/css2?family=Readex+Pro:wght@200;400;600&display=swap`

/* vitepress head */
export const ogUrl = 'https://rpg-docs.netlify.app/'
export const ogImage = `${ogUrl}og.png`

/* GitHub and social links */
export const gitlab = 'https://gitlab.com/ewilan-riviere/rpg-docs'
export const gameOfRolesWiki = 'https://gameofroles.wiki/view/Accueil'
export const elderCraft = 'https://www.elder-craft.com/'

/* Avatar/Image/Sponsors servers */
export const preconnectLinks = [googleapis, gstatic]
export const preconnectHomeLinks = [googleapis, gstatic]

/* PWA runtime caching urlPattern regular expressions */
export const pwaFontsRegex = new RegExp(`^${googleapis}/.*`, 'i')
export const pwaFontStylesRegex = new RegExp(`^${gstatic}/.*`, 'i')
